﻿// <copyright file="IRepository.cs" company="Nagy Dávid O0U6EC">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CraftCamp.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using CraftCamp.Data;

    /// <summary>
    /// IRepository interface, amit minden tábla interface megvalósít.
    /// </summary>
    /// <typeparam name="T">Valamilyen tábla típus.</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// SELECT ALL.
        /// </summary>
        /// <returns>IQueryable Collection T-ket tartalmazó collection.</returns>
        IQueryable<T> Listazas();

        /// <summary>
        /// INSERT for all tables.
        /// </summary>
        /// <param name="entity">Entity.</param>
        void Hozzaadas(T entity);
    }

    /// <summary>
    /// GyerekRepository Interface.
    /// </summary>
    public interface IGyerekRepository : IRepository<Gyerekek>
    {
        /// <summary>
        /// SELECT BY ID for Gyerekek.
        /// </summary>
        /// <param name="id">Gyerek MC neve.</param>
        /// <returns>Gyerek típus.</returns>
        Gyerekek GetOne(string id);

        /// <summary>
        /// UPDATE for MC név.
        /// </summary>
        /// <param name="id">MC név.</param>
        /// <param name="uj_id">Új MC név.</param>
        void MC_Modositas(string id, string uj_id);

        /// <summary>
        /// UPDATE for Nev.
        /// </summary>
        /// <param name="id">MC Nev.</param>
        /// <param name="uj_nev">Új név.</param>
        void Nev_Modositas(string id, string uj_nev);

        /// <summary>
        /// UPDATE for Kor.
        /// </summary>
        /// <param name="id">MC nev.</param>
        /// <param name="uj_kor">Új kor.</param>
        void Kor_Modositas(string id, string uj_kor);

        /// <summary>
        /// UPDATE for Szülő_ID.
        /// </summary>
        /// <param name="id">MC név.</param>
        /// <param name="uj_SZ">Új szülő SZ_ID-ja.</param>
        void SZ_Modositas(string id, string uj_SZ);

        /// <summary>
        /// DELETE for Gyerek.
        /// </summary>
        /// <param name="id">MC név.</param>
        void Torles(string id);
    }

    /// <summary>
    /// SzuloRepository Interface.
    /// </summary>
    public interface ISzuloRepository : IRepository<Szulok>
    {
        /// <summary>
        /// SELECT BY ID for Szulo.
        /// </summary>
        /// <param name="id">Szülő SZ_ID.</param>
        /// <returns>Szulok típus.</returns>
        Szulok GetOne(string id);

        /// <summary>
        /// UPDATE for SZ_ID.
        /// </summary>
        /// <param name="id">Szülő SZ_ID.</param>
        /// <param name="uj_id">Szülő új SZ_ID.</param>
        void ID_Modositas(string id, string uj_id);

        /// <summary>
        /// UPDATE for nev.
        /// </summary>
        /// <param name="id">Szülő SZ_ID.</param>
        /// <param name="uj_nev">új név.</param>
        void Nev_Modositas(string id, string uj_nev);

        /// <summary>
        /// UPDATE for telefonszam.
        /// </summary>
        /// <param name="id">Szülő SZ_ID.</param>
        /// <param name="uj_telefon">új telefonszám.</param>
        void Telefonszam_Modositas(string id, string uj_telefon);

        /// <summary>
        /// UPDATE for Email.
        /// </summary>
        /// <param name="id">Szülő SZ_ID.</param>
        /// <param name="uj_email">Új email.</param>
        void Email_Modositas(string id, string uj_email);

        /// <summary>
        /// UPDATE for Telepules.
        /// </summary>
        /// <param name="id">Szülő SZ_ID.</param>
        /// <param name="uj_telepules">Új település.</param>
        void Telepules_Modositas(string id, string uj_telepules);

        /// <summary>
        /// UPDATE for Hirlevel.
        /// </summary>
        /// <param name="id">Szülő SZ_ID.</param>
        /// <param name="uj_hirlevel">Új hírlevél státusz.</param>
        void Hirlevel_Modositas(string id, short uj_hirlevel);

        /// <summary>
        /// DELETE for Szulok.
        /// </summary>
        /// <param name="id">Szülő SZ_ID.</param>
        void Torles(string id);
    }

    /// <summary>
    /// TaborRepository Interface.
    /// </summary>
    public interface ITaborRepository : IRepository<Taborok>
    {
        /// <summary>
        /// SELECT BY ID for Taborok.
        /// </summary>
        /// <param name="id">Tabor ID.</param>
        /// <returns>Taborok típus.</returns>
        Taborok GetOne(long id);

        /// <summary>
        /// UPDATE for TaborID.
        /// </summary>
        /// <param name="id">Tabor ID.</param>
        /// <param name="uj_id">Új Tábor ID.</param>
        void ID_Modositas(long id, long uj_id);

        /// <summary>
        /// UPDATE for Nev.
        /// </summary>
        /// <param name="id">Tábor ID.</param>
        /// <param name="uj_nev">Új név.</param>
        void Nev_Modositas(long id, string uj_nev);

        /// <summary>
        /// UPDATE for Kezdés.
        /// </summary>
        /// <param name="id">Tábor ID.</param>
        /// <param name="uj_kezdes">Új kezdés.</param>
        void Kezdes_Modositas(long id, DateTime uj_kezdes);

        /// <summary>
        /// UPDATE for Befejezes.
        /// </summary>
        /// <param name="id">Tábor ID.</param>
        /// <param name="uj_befejezes">Új befejezés.</param>
        void Befejezes_Modositas(long id, DateTime uj_befejezes);

        /// <summary>
        /// UPDATE for SzabadHelyek.
        /// </summary>
        /// <param name="id">Tábor ID.</param>
        /// <param name="uj_szabadHelyek">Új szabadhelyek.</param>
        void SzabadHelyek_Modositas(long id, long uj_szabadHelyek);

        /// <summary>
        /// UPDATE for Maxfo.
        /// </summary>
        /// <param name="id">Tábor ID.</param>
        /// <param name="uj_maxfo">Új maxfő.</param>
        void Maxfo_Modositas(long id, long uj_maxfo);

        /// <summary>
        /// UPDATE for helyszínID.
        /// </summary>
        /// <param name="id">Tábor ID.</param>
        /// <param name="uj_hID">Új helyszínID.</param>
        void HID_Modositas(long id, long uj_hID);

        /// <summary>
        /// DELETE for Táborok.
        /// </summary>
        /// <param name="id">TáborID.</param>
        void Torles(long id);
    }

    /// <summary>
    /// HelyszinRepository Interface.
    /// </summary>
    public interface IHelyszinRepository : IRepository<Helyszinek>
    {
        /// <summary>
        /// SELECT BY ID for Helyszínek.
        /// </summary>
        /// <param name="id">Helyszín ID.</param>
        /// <returns>Helyszínek típus.</returns>
        Helyszinek GetOne(long id);

        /// <summary>
        /// UPDATE for ID.
        /// </summary>
        /// <param name="id">Helyszín ID.</param>
        /// <param name="uj_id">Új Helyszín ID.</param>
        void ID_Modositas(long id, long uj_id);

        /// <summary>
        /// UPDATE for Nev.
        /// </summary>
        /// <param name="id">Helyszín ID.</param>
        /// <param name="uj_nev">Új név.</param>
        void Nev_Modositas(long id, string uj_nev);

        /// <summary>
        /// UPDATE for Település.
        /// </summary>
        /// <param name="id">Helyszín ID.</param>
        /// <param name="uj_telepules">Új település.</param>
        void Telepules_Modositas(long id, string uj_telepules);

        /// <summary>
        /// UPDATE for Utca.
        /// </summary>
        /// <param name="id">Helyszín ID.</param>
        /// <param name="uj_utca">Új utca.</param>
        void Utca_Modositas(long id, string uj_utca);

        /// <summary>
        /// UPDATE for Telefonszam.
        /// </summary>
        /// <param name="id">Helyszín ID.</param>
        /// <param name="uj_telefon">Új telefonszám.</param>
        void Telefonszam_Modositas(long id, string uj_telefon);

        /// <summary>
        /// UPDATE for Maxhely.
        /// </summary>
        /// <param name="id">Helyszín ID.</param>
        /// <param name="uj_maxhely">Új maxhely.</param>
        void Maxhely_Modositas(long id, long uj_maxhely);

        /// <summary>
        /// DELETE for Helyszinek.
        /// </summary>
        /// <param name="id">Helyszín ID.</param>
        /// <returns>TRUE/FALSE based on success or failure.</returns>
        bool Torles(long id);
    }
}
