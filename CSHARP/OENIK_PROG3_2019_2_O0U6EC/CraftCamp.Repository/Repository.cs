﻿// <copyright file="Repository.cs" company="Nagy Dávid O0U6EC">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CraftCamp.Repository
{
    using System.Data.Entity;
    using System.Linq;

    /// <summary>
    /// Fő repository, amely megvalósítja az IRepository Interfacet.
    /// </summary>
    /// <typeparam name="T">Valamely Entity típus.</typeparam>
    public abstract class Repository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// DbContext.
        /// </summary>
        private DbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// </summary>
        /// <param name="ctx">DbContext.</param>
        public Repository(DbContext ctx)
        {
            this.Ctx = ctx;
        }

        /// <summary>
        /// Gets or Sets private DbContext.
        /// </summary>
        protected DbContext Ctx { get => this.ctx; set => this.ctx = value; }

        /// <summary>
        /// SELECT ALL.
        /// </summary>
        /// <returns>IQueryable T type collection.</returns>
        public IQueryable<T> Listazas()
        {
            return this.Ctx.Set<T>();
        }

        /// <summary>
        /// Add new entity.
        /// </summary>
        /// <param name="entity">entity type.</param>
        public void Hozzaadas(T entity)
        {
            this.ctx.Set<T>().Add(entity);

            // this.ctx.Database.ExecuteSqlCommand(@"SET IDENTITY_INSERT [dbo].[Helyszinek] ON");
            this.ctx.SaveChanges();
        }
    }
}
