﻿// <copyright file="GyerekRepository.cs" company="Nagy Dávid O0U6EC">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CraftCamp.Repository
{
    using System.Data.Entity;
    using System.Linq;
    using CraftCamp.Data;

    /// <summary>
    /// GyerekRepository.
    /// </summary>
    public class GyerekRepository : Repository<Gyerekek>, IGyerekRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GyerekRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext.</param>
        public GyerekRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// SELECT BY ID for Gyerekek.
        /// </summary>
        /// <param name="id">Gyerek MC neve.</param>
        /// <returns>Kiválasztott Gyerek.</returns>
        public Gyerekek GetOne(string id)
        {
            return this.Listazas().SingleOrDefault(x => x.MC_USER == id);
        }

        /// <summary>
        /// UPDATE for Kor.
        /// </summary>
        /// <param name="id">Gyerek MC neve.</param>
        /// <param name="uj_kor">Új kor.</param>
        public void Kor_Modositas(string id, string uj_kor)
        {
            Gyerekek gyerek = this.GetOne(id);
            gyerek.Kor = uj_kor;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE for MC nev.
        /// </summary>
        /// <param name="id">Gyerek MC neve.</param>
        /// <param name="uj_id">Gyerek új MC neve.</param>
        public void MC_Modositas(string id, string uj_id)
        {
            Gyerekek gyerek = this.GetOne(id);
            gyerek.MC_USER = uj_id;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE for Nev.
        /// </summary>
        /// <param name="id">Gyerek MC neve.</param>
        /// <param name="uj_nev">Gyerek új neve.</param>
        public void Nev_Modositas(string id, string uj_nev)
        {
            Gyerekek gyerek = this.GetOne(id);
            gyerek.Nev = uj_nev;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE for Gyerek Szülő SZ_ID.
        /// </summary>
        /// <param name="id">Gyerek MC neve.</param>
        /// <param name="uj_SZ">Gyerek új szülőjének SZ_ID-ja.</param>
        public void SZ_Modositas(string id, string uj_SZ)
        {
            Gyerekek gyerek = this.GetOne(id);
            gyerek.SZ_ID = uj_SZ;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// DELETE for Gyerek.
        /// </summary>
        /// <param name="id">Gyerek ID.</param>
        public void Torles(string id)
        {
            Gyerekek gyerek = this.GetOne(id);
            (this.Ctx as CraftCampDBEntities).Gyerekek.Remove(gyerek);
            this.Ctx.SaveChanges();
        }
    }
}
