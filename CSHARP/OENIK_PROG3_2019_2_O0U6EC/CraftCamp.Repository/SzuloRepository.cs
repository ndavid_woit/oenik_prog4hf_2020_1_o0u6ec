﻿// <copyright file="SzuloRepository.cs" company="Nagy Dávid O0U6EC">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CraftCamp.Repository
{
    using System.Data.Entity;
    using System.Linq;
    using CraftCamp.Data;

    /// <summary>
    /// SzuloRepository.
    /// </summary>
    public class SzuloRepository : Repository<Szulok>, ISzuloRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SzuloRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext.</param>
        public SzuloRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// UPDATE for Email.
        /// </summary>
        /// <param name="id">Szülő ID.</param>
        /// <param name="uj_email">Új email.</param>
        public void Email_Modositas(string id, string uj_email)
        {
            Szulok szulo = this.GetOne(id);
            szulo.Email = uj_email;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// SELECT BY ID for Szulok.
        /// </summary>
        /// <param name="id">SZ_ID.</param>
        /// <returns>A kiválasztott Szülő.</returns>
        public Szulok GetOne(string id)
        {
            return this.Listazas().SingleOrDefault(x => x.SZ_ID == id);
        }

        /// <summary>
        /// UPDATE for Hírlevél.
        /// </summary>
        /// <param name="id">SZ_ID.</param>
        /// <param name="uj_hirlevel">Új hírlevél státusz.</param>
        public void Hirlevel_Modositas(string id, short uj_hirlevel)
        {
            Szulok szulo = this.GetOne(id);
            szulo.Hirlevel = uj_hirlevel;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE for SZ_ID.
        /// </summary>
        /// <param name="id">SZ_ID.</param>
        /// <param name="uj_id">Új SZ_ID.</param>
        public void ID_Modositas(string id, string uj_id)
        {
            Szulok szulo = this.GetOne(id);
            szulo.SZ_ID = uj_id;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE for Nev.
        /// </summary>
        /// <param name="id">SZ_ID.</param>
        /// <param name="uj_nev">Új név.</param>
        public void Nev_Modositas(string id, string uj_nev)
        {
            Szulok szulo = this.GetOne(id);
            szulo.Nev = uj_nev;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE for Telefonszam.
        /// </summary>
        /// <param name="id">SZ_ID.</param>
        /// <param name="uj_telefon">Új telefonszám.</param>
        public void Telefonszam_Modositas(string id, string uj_telefon)
        {
            Szulok szulo = this.GetOne(id);
            szulo.Telefonszam = uj_telefon;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE for Település.
        /// </summary>
        /// <param name="id">SZ_ID.</param>
        /// <param name="uj_telepules">Új település.</param>
        public void Telepules_Modositas(string id, string uj_telepules)
        {
            Szulok szulo = this.GetOne(id);
            szulo.Telepules = uj_telepules;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// DELETE for Szulok.
        /// </summary>
        /// <param name="id">SZ_ID.</param>
        public void Torles(string id)
        {
            Szulok szulo = this.GetOne(id);
            (this.Ctx as CraftCampDBEntities).Szulok.Remove(szulo);
            this.Ctx.SaveChanges();
        }
    }
}
