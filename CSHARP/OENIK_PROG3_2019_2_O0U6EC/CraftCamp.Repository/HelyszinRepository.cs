﻿// <copyright file="HelyszinRepository.cs" company="Nagy Dávid O0U6EC">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CraftCamp.Repository
{
    using System.Data.Entity;
    using System.Linq;
    using CraftCamp.Data;

    /// <summary>
    /// HelyszinRepository.
    /// </summary>
    public class HelyszinRepository : Repository<Helyszinek>, IHelyszinRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HelyszinRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext.</param>
        public HelyszinRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// SELECT BY ID for Helyszinek.
        /// </summary>
        /// <param name="id">HelyszinID.</param>
        /// <returns>A kiválasztott Helyszin.</returns>
        public Helyszinek GetOne(long id)
        {
            return this.Listazas().SingleOrDefault(x => x.ID == id);
        }

        /// <summary>
        /// UPDATE for HelyszínID.
        /// </summary>
        /// <param name="id">Helyszín ID.</param>
        /// <param name="uj_id">Új Helyszín ID.</param>
        public void ID_Modositas(long id, long uj_id)
        {
            Helyszinek helyszin = this.GetOne(id);
            helyszin.ID = uj_id;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE for Maxhely.
        /// </summary>
        /// <param name="id">Helyszín ID.</param>
        /// <param name="uj_maxhely">Új maxhely.</param>
        public void Maxhely_Modositas(long id, long uj_maxhely)
        {
            Helyszinek helyszin = this.GetOne(id);
            helyszin.Maxhely = uj_maxhely;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE for Nev.
        /// </summary>
        /// <param name="id">Helyszín ID.</param>
        /// <param name="uj_nev">Új név.</param>
        public void Nev_Modositas(long id, string uj_nev)
        {
            Helyszinek helyszin = this.GetOne(id);
            helyszin.Nev = uj_nev;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE for Telefonszam.
        /// </summary>
        /// <param name="id">Helyszín ID.</param>
        /// <param name="uj_telefon">Új Telefonszám.</param>
        public void Telefonszam_Modositas(long id, string uj_telefon)
        {
            Helyszinek helyszin = this.GetOne(id);
            helyszin.Telefonszam = uj_telefon;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE for Település.
        /// </summary>
        /// <param name="id">Helyszín ID.</param>
        /// <param name="uj_telepules">Új település.</param>
        public void Telepules_Modositas(long id, string uj_telepules)
        {
            Helyszinek helyszin = this.GetOne(id);
            helyszin.Telepules = uj_telepules;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// DELETE for Helyszín.
        /// </summary>
        /// <param name="id">Helyszín ID.</param>
        /// <returns>TRUE/FALSE based on success or failure.</returns>
        public bool Torles(long id)
        {
            Helyszinek helyszin = this.GetOne(id);
            if (helyszin == null)
            {
                return false;
            }

            (this.Ctx as CraftCampDBEntities).Helyszinek.Remove(helyszin);
            this.Ctx.SaveChanges();
            return true;
        }

        /// <summary>
        /// UPDATE for Utca.
        /// </summary>
        /// <param name="id">Helyszín ID.</param>
        /// <param name="uj_utca">Új utca.</param>
        public void Utca_Modositas(long id, string uj_utca)
        {
            Helyszinek helyszin = this.GetOne(id);
            helyszin.Utca = uj_utca;
            this.Ctx.SaveChanges();
        }
    }
}
