﻿// <copyright file="TaborRepository.cs" company="Nagy Dávid O0U6EC">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CraftCamp.Repository
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using CraftCamp.Data;

    /// <summary>
    /// TaborRepository.
    /// </summary>
    public class TaborRepository : Repository<Taborok>, ITaborRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaborRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext.</param>
        public TaborRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// UPDATE for Befejezes.
        /// </summary>
        /// <param name="id">Tabor ID.</param>
        /// <param name="uj_befejezes">Új befejezési dátum.</param>
        public void Befejezes_Modositas(long id, DateTime uj_befejezes)
        {
            Taborok tabor = this.GetOne(id);
            tabor.Befejezes = uj_befejezes;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// SELECT BY ID for Taborok.
        /// </summary>
        /// <param name="id">Tabor ID.</param>
        /// <returns>A kiválasztott Tábor.</returns>
        public Taborok GetOne(long id)
        {
            return this.Listazas().SingleOrDefault(x => x.ID == id);
        }

        /// <summary>
        /// UPDATE for HelyszínID.
        /// </summary>
        /// <param name="id">Tábor ID.</param>
        /// <param name="uj_hID">Új Helyszín ID.</param>
        public void HID_Modositas(long id, long uj_hID)
        {
            Taborok tabor = this.GetOne(id);
            tabor.H_ID = uj_hID;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE for Tábor ID.
        /// </summary>
        /// <param name="id">Tábor ID.</param>
        /// <param name="uj_id">Tábor új ID.</param>
        public void ID_Modositas(long id, long uj_id)
        {
            Taborok tabor = this.GetOne(id);
            tabor.ID = uj_id;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE for Kezdes.
        /// </summary>
        /// <param name="id">Tábor ID.</param>
        /// <param name="uj_kezdes">Új kezdés.</param>
        public void Kezdes_Modositas(long id, DateTime uj_kezdes)
        {
            Taborok tabor = this.GetOne(id);
            tabor.Kezdes = uj_kezdes;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE for Maxfo.
        /// </summary>
        /// <param name="id">Tábor ID.</param>
        /// <param name="uj_maxfo">Új maxfő.</param>
        public void Maxfo_Modositas(long id, long uj_maxfo)
        {
            Taborok tabor = this.GetOne(id);
            tabor.Maxfo = uj_maxfo;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE for Nev.
        /// </summary>
        /// <param name="id">Tábor ID.</param>
        /// <param name="uj_nev">Új név.</param>
        public void Nev_Modositas(long id, string uj_nev)
        {
            Taborok tabor = this.GetOne(id);
            tabor.Nev = uj_nev;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// UPDATE for SzabadHelyek.
        /// </summary>
        /// <param name="id">Tábor ID.</param>
        /// <param name="uj_szabadHelyek">Új szabadHelyek.</param>
        public void SzabadHelyek_Modositas(long id, long uj_szabadHelyek)
        {
            Taborok tabor = this.GetOne(id);
            tabor.Szabad_helyek = uj_szabadHelyek;
            this.Ctx.SaveChanges();
        }

        /// <summary>
        /// DELETE for Taborok.
        /// </summary>
        /// <param name="id">Tábor ID.</param>
        public void Torles(long id)
        {
            Taborok tabor = this.GetOne(id);
            (this.Ctx as CraftCampDBEntities).Taborok.Remove(tabor);
            this.Ctx.SaveChanges();
        }
    }
}
