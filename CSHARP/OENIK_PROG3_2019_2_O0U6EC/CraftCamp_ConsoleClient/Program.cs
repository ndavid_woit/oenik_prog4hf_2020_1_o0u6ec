﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace CraftCamp_ConsoleClient
{
    public class Places
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public int Capacity { get; set; }
        public string City { get; set; }
        public string Street { get; set; }

        public override string ToString()
        {
            return $"ID: {ID}; Name: {Name}; Phone: {Phone}; Capacity: {Capacity}; City: {City}; Street: {Street}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("...WAITING...");
            Console.ReadLine();

            string url = "http://localhost:60631/api/PlacesApi/";

            using(HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Places>>(json);
                list.ForEach(item => Console.WriteLine(item) );
                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Places.Name), "ApiHely");
                postData.Add(nameof(Places.Phone), "+36/666666");
                postData.Add(nameof(Places.Capacity), "47");
                postData.Add(nameof(Places.City), "ApiVaros");
                postData.Add(nameof(Places.Street), "ApiUtcaaa");
                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD " + response );
                Console.WriteLine("ALL " + json );
                Console.ReadLine();


                int placeID = JsonConvert.DeserializeObject<List<Places>>(json).SingleOrDefault(x => x.City == "ApiVaros").ID;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Places.ID), placeID.ToString());
                postData.Add(nameof(Places.Name), "ApiHely_Modositott");
                postData.Add(nameof(Places.Phone), "+36/666666");
                postData.Add(nameof(Places.Capacity), "47");
                postData.Add(nameof(Places.City), "ApiVaros_Modositott");
                postData.Add(nameof(Places.Street), "ApiUtcaaa");
                response = client.PostAsync(url + "edit", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("EDIT " + response);
                Console.WriteLine("ALL " + json);
                Console.ReadLine();


                response = client.GetStringAsync(url + "delete/" + placeID).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DELETE " + response);
                Console.WriteLine("ALL " + json);
            }
        }
    }
}
