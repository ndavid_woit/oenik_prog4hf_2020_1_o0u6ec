﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CraftCamp.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CraftCamp.Data.Helyszinek, CraftCamp.Web.Models.Places>().
                    ForMember(dest => dest.ID, map => map.MapFrom(src => src.ID)).
                    ForMember(dest => dest.Name, map => map.MapFrom(src => src.Nev)).
                    ForMember(dest => dest.Phone, map => map.MapFrom(src => src.Telefonszam)).
                    ForMember(dest => dest.Capacity, map => map.MapFrom(src => src.Maxhely)).
                    ForMember(dest => dest.City, map => map.MapFrom(src => src.Telepules)).
                    ForMember(dest => dest.Street, map => map.MapFrom(src => src.Utca));
            });
            return config.CreateMapper();
        }

        public static IMapper CreateMapperReverse()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CraftCamp.Data.Helyszinek, CraftCamp.Web.Models.Places>().
                    ForMember(dest => dest.ID, map => map.MapFrom(src => src.ID)).
                    ForMember(dest => dest.Name, map => map.MapFrom(src => src.Nev)).
                    ForMember(dest => dest.Phone, map => map.MapFrom(src => src.Telefonszam)).
                    ForMember(dest => dest.Capacity, map => map.MapFrom(src => src.Maxhely)).
                    ForMember(dest => dest.City, map => map.MapFrom(src => src.Telepules)).
                    ForMember(dest => dest.Street, map => map.MapFrom(src => src.Utca)).
                    ReverseMap();
            });
            return config.CreateMapper();
        }
    }
}