﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CraftCamp.Web.Models
{
    public class Places
    {
        [Display(Name="Place ID")]
        [Required]
        public int ID { get; set; }

        [Display(Name = "Place Name")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Place Phone number")]
        [Required]
        public string Phone { get; set; }

        [Display(Name = "Place Capacity")]
        [Required]
        public int Capacity { get; set; }

        [Display(Name = "City of the Place")]
        [Required]
        public string City { get; set; }

        [Display(Name = "Street of the Place")]
        [Required]
        public string Street { get; set; }
    }
}