﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CraftCamp.Web.Models
{
    public class PlacesViewModel
    {
        public Places newPlace { get; set; }
        public Places editedPlace { get; set; }
        public List<Places> ListOfPlaces { get; set; }
    }
}