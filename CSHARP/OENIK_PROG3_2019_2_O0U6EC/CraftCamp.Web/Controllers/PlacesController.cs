﻿using AutoMapper;
using CraftCamp.Data;
using CraftCamp.Logic;
using CraftCamp.Repository;
using CraftCamp.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CraftCamp.Web.Controllers
{
    public class PlacesController : Controller
    {
        IHelyszinLogic hLogic;
        PlacesViewModel model;
        IMapper mapper;
        IMapper mapperReverse;
        public PlacesController()
        {
            CraftCampDBEntities ctx = new CraftCampDBEntities();
            HelyszinRepository repo = new HelyszinRepository(ctx);
            hLogic = new HelyszinLogic(repo);
            mapper = MapperFactory.CreateMapper();
            mapperReverse = MapperFactory.CreateMapperReverse();
            model = new PlacesViewModel();
            model.editedPlace = new Places();
            model.newPlace = new Places();
            var places = hLogic.Listazas().ToList();
            model.ListOfPlaces = mapper.Map<List<CraftCamp.Data.Helyszinek>, List<CraftCamp.Web.Models.Places>>(places);
        }

        private Places GetPlaceModel(int id)
        {
            Helyszinek onePlace = hLogic.GetOne(id);
            return mapper.Map<Helyszinek, Places>(onePlace);
        }

        // GET: Places
        public ActionResult Index()
        {
            model.newPlace = new Places();
            return View("PlacesIndex",model);
        }

        // GET: Places/Details/5
        public ActionResult Details(int id)
        {
            return View("PlacesDetails",GetPlaceModel(id));
        }

        [HttpPost]
        public ActionResult Add(Places place)
        {
            Helyszinek ujHelyszin = mapperReverse.Map<Places, Helyszinek>(place);
            int count = hLogic.Listazas().Count();
            
            hLogic.Hozzaadas(ujHelyszin);
            model.newPlace = new Places();

            if (count < hLogic.Listazas().Count())
                TempData["editResult"] = "Insert OK!";
            else
                TempData["editResult"] = "Insert FAILED!";
            return RedirectToAction("Index", model);
        }

        // GET: Places/Edit/5
        public ActionResult Edit(int id)
        {
            model.editedPlace = GetPlaceModel(id);
            return View("PlacesIndex", model);
        }

        [HttpPost]
        public ActionResult Edit(Places place)
        {
            Helyszinek originalHelyszin = hLogic.GetOne(place.ID);
            Helyszinek modHelyszin = mapperReverse.Map<Places, Helyszinek>(place);

            if (originalHelyszin.Nev != modHelyszin.Nev)
                hLogic.Nev_Modositas(place.ID, place.Name);
            if (originalHelyszin.Maxhely != modHelyszin.Maxhely)
                hLogic.Maxhely_Modositas(place.ID, place.Capacity);
            if (originalHelyszin.Telefonszam != modHelyszin.Telefonszam)
                hLogic.Telefonszam_Modositas(place.ID, place.Phone);
            if (originalHelyszin.Telepules != modHelyszin.Telepules)
                hLogic.Telepules_Modositas(place.ID, modHelyszin.Telepules);
            if (originalHelyszin.Utca != modHelyszin.Utca)
                hLogic.Utca_Modositas(place.ID, modHelyszin.Utca);

            model.editedPlace = new Places();
            TempData["editResult"] = "Edit OK!";
            return RedirectToAction("Index", model);
        }

        // GET: Places/Delete/5
        public ActionResult Delete(int id)
        {
            if(hLogic.Torles(id))
                TempData["editResult"] = "Delete OK!";
            else
                TempData["editResult"] = "Delete FAIL!";
            return RedirectToAction("Index");
        }
    }
}
