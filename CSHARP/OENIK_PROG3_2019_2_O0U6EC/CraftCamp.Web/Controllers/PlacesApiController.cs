﻿using AutoMapper;
using CraftCamp.Data;
using CraftCamp.Logic;
using CraftCamp.Repository;
using CraftCamp.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CraftCamp.Web.Controllers
{
    public class PlacesApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        IHelyszinLogic hLogic;
        IMapper mapper;
        IMapper mapperReverse;
        public PlacesApiController()
        {
            CraftCampDBEntities ctx = new CraftCampDBEntities();
            HelyszinRepository repo = new HelyszinRepository(ctx);
            hLogic = new HelyszinLogic(repo);
            mapper = MapperFactory.CreateMapper();
            mapperReverse = MapperFactory.CreateMapperReverse();
        }

        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Places> GetAll()
        {
            var places = hLogic.Listazas().ToList();
            return mapper.Map<IList<Helyszinek>, List<Places>>(places);
        }

        [ActionName("delete")]
        [HttpGet]
        public ApiResult Delete(int id)
        {
            return new ApiResult() { OperationResult = hLogic.Torles(id) };
        }

        [ActionName("add")]
        [HttpPost]
        public ApiResult Add(Places place)
        {
            Helyszinek helyszin = mapperReverse.Map<Places, Helyszinek>(place);
            hLogic.Hozzaadas(helyszin);
            return new ApiResult() { OperationResult = true };
        }

        [HttpPost]
        [ActionName("edit")]
        public ApiResult Edit(Places place)
        {
            Helyszinek originalHelyszin = hLogic.GetOne(place.ID);
            Helyszinek modHelyszin = mapperReverse.Map<Places, Helyszinek>(place);

            if (originalHelyszin.Nev != modHelyszin.Nev)
                hLogic.Nev_Modositas(place.ID, place.Name);
            if (originalHelyszin.Maxhely != modHelyszin.Maxhely)
                hLogic.Maxhely_Modositas(place.ID, place.Capacity);
            if (originalHelyszin.Telefonszam != modHelyszin.Telefonszam)
                hLogic.Telefonszam_Modositas(place.ID, place.Phone);
            if (originalHelyszin.Telepules != modHelyszin.Telepules)
                hLogic.Telepules_Modositas(place.ID, modHelyszin.Telepules);
            if (originalHelyszin.Utca != modHelyszin.Utca)
                hLogic.Utca_Modositas(place.ID, modHelyszin.Utca);

            return new ApiResult() { OperationResult = true };
        }
    }
}
