﻿// <copyright file="Program.cs" company="Nagy Dávid O0U6EC">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CraftCamp.Program
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using CraftCamp.Data;
    using CraftCamp.Logic;

    /// <summary>
    /// Főosztály.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Főprogram. Létrehozza a Logicokat és elindítja a menüt.
        /// </summary>
        /// <param name="args">args.</param>
        public static void Main(string[] args)
        {
            NonCRUDLogic logic = new NonCRUDLogic();
            GyerekLogic gyLogic = new GyerekLogic();
            HelyszinLogic hLogic = new HelyszinLogic();
            SzuloLogic szLogic = new SzuloLogic();
            TaborLogic tLogic = new TaborLogic();
            Menu(gyLogic, szLogic, tLogic, hLogic, logic);
        }

        private static void Menu(GyerekLogic gyLogic, SzuloLogic szLogic, TaborLogic tLogic, HelyszinLogic hLogic, NonCRUDLogic logic)
        {
            Console.WriteLine("MENÜ");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("11 - Gyerekek kilistázása");
            Console.WriteLine("111 - 1 Gyerek kilistázása");
            Console.WriteLine("12 - Gyerek hozzáadása");
            Console.WriteLine("13 - Gyerek MC nevének(PK) módosítása");
            Console.WriteLine("14 - Gyerek nevének módosítása");
            Console.WriteLine("15 - Gyerek korának módosítása");
            Console.WriteLine("16 - Gyerek szülőjének(ID) módosítása (nincs meg)");
            Console.WriteLine("17 - Gyerek törlése");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("21 - Szülők kilistázása");
            Console.WriteLine("211 - 1 Szülő kilistázása");
            Console.WriteLine("22 - Szülő hozzáadása");
            Console.WriteLine("23 - Szülő nevének módosítása");
            Console.WriteLine("24 - Szülő telefonszámának módosítása");
            Console.WriteLine("25 - Szülő e-mail címének módosítása");
            Console.WriteLine("26 - Szülő településének módosítása");
            Console.WriteLine("27 - Szülő hírlevél státuszának módosítása");
            Console.WriteLine("28 - Szülő ID módosítása (nincs meg)");
            Console.WriteLine("29 - Szülő törlése");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("31 - Táborok kilistázása");
            Console.WriteLine("311 - 1 Tábor kilistázása");
            Console.WriteLine("32 - Tábor hozzáadása");
            Console.WriteLine("33 - Tábor nevének módosítása");
            Console.WriteLine("34 - Tábor kezdési időpontjának módosítása");
            Console.WriteLine("35 - Tábor befejezési időpontjának módosítása");
            Console.WriteLine("36 - Tábor szabad helyeinek módosítása");
            Console.WriteLine("37 - Tábor maximális létszámának módosítása");
            Console.WriteLine("38 - Tábor helyszínének(H_ID) módosítása (nincs meg)");
            Console.WriteLine("39 - Tábor törlése");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("41 - Helyszínek kilistázása");
            Console.WriteLine("411 - 1 Helyszín kilistázása");
            Console.WriteLine("42 - Helyszín hozzáadása");
            Console.WriteLine("43 - Helyszín nevének módosítása");
            Console.WriteLine("44 - Helyszín településének módosítása");
            Console.WriteLine("45 - Helyszín utcájának módosítása");
            Console.WriteLine("46 - Helyszín telefonszámának módosítása");
            Console.WriteLine("47 - Helyszín maximális befogadóképességének módosítása");
            Console.WriteLine("48 - Helyszín törlése");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("51 - Listázzuk ki azokat a szülőket, akik fel vannak hírlevélre iratkozva.");
            Console.WriteLine("52 - Listázzuk ki az egyes tábortípusokat és azt, hogy ezek átlagosan milyen maximális létszámmal indultak.");
            Console.WriteLine("53 - Listázzuk ki azt, hogy az egyes táborok mely helyszíneken indulnak.");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("60 - Generáljunk új gyerekeket egy szülőhöz - Java végpont");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("MENÜ VÉGE");

            MenuKezeles(gyLogic, szLogic, tLogic, hLogic, logic);
        }

        private static void MenuKezeles(GyerekLogic gyLogic, SzuloLogic szLogic, TaborLogic tLogic, HelyszinLogic hLogic, NonCRUDLogic logic)
        {
            string selected = Console.ReadLine();
            while (selected != "exit")
            {
                if (selected == "11")
                {
                    Console.WriteLine("---------GYEREKEK LISTÁZÁSÁNAK KEZDETE----------");
                    GyerekListazas(gyLogic);
                    Console.WriteLine("---------GYEREKEK LISTÁZÁSÁNAK VÉGE-------------");
                }
                else if (selected == "111")
                {
                    Console.WriteLine("-------GYEREK-------");
                    Console.WriteLine("Kérem a gyerek MC_nevét: ");
                    string id = Console.ReadLine();
                    var gyerek = gyLogic.GetOne(id);
                    Console.WriteLine(gyerek.ToString());
                    Console.WriteLine("-----GYEREK VÉGE---------");
                }
                else if (selected == "12")
                {
                    Console.WriteLine("--------ÚJ GYEREK HOZZÁADÁSA------");
                    Console.WriteLine("Kérem a gyerek Minecraft felhasználónevét, nevét, korát, szülő ID-ját(vesszővel elválasztva): ");
                    string[] adatok = Console.ReadLine().Split(',');
                    Gyerekek ujGyerek = new Gyerekek()
                    {
                        MC_USER = adatok[0],
                        Nev = adatok[1],
                        Kor = adatok[2],
                        SZ_ID = adatok[3],
                    };
                    GyerekHozzaadas(gyLogic, ujGyerek);
                    Console.WriteLine("--------ÚJ GYEREK HOZZÁADÁSA VÉGE------");
                }
                else if (selected == "13")
                {
                    Console.WriteLine("-------GYEREK MC FELHASZNÁLÓJÁNAK MÓDOSÍTÁSA-----------");
                    Console.WriteLine("Kérem a gyerek MC_nevét: ");
                    string id = Console.ReadLine();
                    Console.WriteLine("Kérem a gyerek új MC_nevét: ");
                    string uj_id = Console.ReadLine();
                    GyerekIdModositas(gyLogic, id, uj_id);
                    Console.WriteLine("-------GYEREK MC FELHASZNÁLÓJÁNAK MÓDOSÍTÁSA VÉGE-----------");
                }
                else if (selected == "14")
                {
                    Console.WriteLine("-------GYEREK NEVÉNEK MÓDOSÍTÁSA-----------");
                    Console.WriteLine("Kérem a gyerek MC_nevét: ");
                    string id = Console.ReadLine();
                    Console.WriteLine("Kérem a gyerek új nevét: ");
                    string uj_nev = Console.ReadLine();
                    GyerekNevModositas(gyLogic, id, uj_nev);
                    Console.WriteLine("-------GYEREK NEVÉNEK MÓDOSÍTÁSA VÉGE-----------");
                }
                else if (selected == "15")
                {
                    Console.WriteLine("-------GYEREK KORÁNAK MÓDOSÍTÁSA-----------");
                    Console.WriteLine("Kérem a gyerek MC_nevét: ");
                    string id = Console.ReadLine();
                    Console.WriteLine("Kérem a gyerek új korát: ");
                    string uj_kor = Console.ReadLine();
                    GyerekKorModositas(gyLogic, id, uj_kor);
                    Console.WriteLine("-------GYEREK KORÁNAK MÓDOSÍTÁSA VÉGE-----------");
                }
                else if (selected == "17")
                {
                    Console.WriteLine("-------GYEREK TÖRLÉSE-----------");
                    Console.WriteLine("Kérem a gyerek MC_nevét: ");
                    string id = Console.ReadLine();
                    GyerekTorles(gyLogic, id);
                    Console.WriteLine("-------GYEREK TÖRLÉSE VÉGE-----------");
                }
                else if (selected == "21")
                {
                    Console.WriteLine("---------SZÜLŐK LISTÁZÁSÁNAK KEZDETE----------");
                    SzuloListazas(szLogic);
                    Console.WriteLine("---------SZÜLŐK LISTÁZÁSÁNAK VÉGE-------------");
                }
                else if (selected == "211")
                {
                    Console.WriteLine("-------SZÜLŐ-------");
                    Console.WriteLine("Kérem a szülő SZ_ID-t: ");
                    string id = Console.ReadLine();
                    var szulo = szLogic.GetOne(id);
                    Console.WriteLine(szulo.ToString());
                    Console.WriteLine("-----SZÜLŐ VÉGE---------");
                }
                else if (selected == "22")
                {
                    Console.WriteLine("--------ÚJ SZÜLŐ HOZZÁADÁSA------");
                    Console.WriteLine("Kérem a szülő Személyi igazolványát, nevét, Telefonszámát, E-mailjét, Települését, Hirlevél státuszát(0 - nem; 1 - igen) (vesszővel elválasztva): ");
                    string[] adatok = Console.ReadLine().Split(',');
                    Szulok ujSzulo = new Szulok()
                    {
                        SZ_ID = adatok[0],
                        Nev = adatok[1],
                        Telefonszam = adatok[2],
                        Email = adatok[3],
                        Telepules = adatok[4],
                        Hirlevel = short.Parse(adatok[5]),
                    };
                    SzuloHozzaadas(szLogic, ujSzulo);
                    Console.WriteLine("--------ÚJ SZÜLŐ HOZZÁADÁSA VÉGE------");
                }
                else if (selected == "23")
                {
                    Console.WriteLine("-------SZÜLŐ NEVÉNEK MÓDOSÍTÁSA-----------");
                    Console.WriteLine("Kérem a szülő SZ_ID-ját: ");
                    string id = Console.ReadLine();
                    Console.WriteLine("Kérem a szülő új nevét: ");
                    string uj_nev = Console.ReadLine();
                    SzuloNevModositas(szLogic, id, uj_nev);
                    Console.WriteLine("-------SZÜLŐ NEVÉNEK MÓDOSÍTÁSA VÉGE-----------");
                }
                else if (selected == "24")
                {
                    Console.WriteLine("-------SZÜLŐ TELEFONSZÁMÁNAK MÓDOSÍTÁSA-----------");
                    Console.WriteLine("Kérem a szülő SZ_ID-ját: ");
                    string id = Console.ReadLine();
                    Console.WriteLine("Kérem a szülő új telefonszámát: ");
                    string uj_telefon = Console.ReadLine();
                    SzuloTelefonModositas(szLogic, id, uj_telefon);
                    Console.WriteLine("-------SZÜLŐ TELEFONSZÁMÁNAK MÓDOSÍTÁSA VÉGE-----------");
                }
                else if (selected == "25")
                {
                    Console.WriteLine("-------SZÜLŐ EMAIL MÓDOSÍTÁSA-----------");
                    Console.WriteLine("Kérem a szülő SZ_ID-ját: ");
                    string id = Console.ReadLine();
                    Console.WriteLine("Kérem a szülő új EMAIL-jét: ");
                    string uj_email = Console.ReadLine();
                    SzuloEmailModositas(szLogic, id, uj_email);
                    Console.WriteLine("-------SZÜLŐ EMAIL MÓDOSÍTÁSA VÉGE-----------");
                }
                else if (selected == "26")
                {
                    Console.WriteLine("-------SZÜLŐ TELEPÜLÉSÉNEK MÓDOSÍTÁSA-----------");
                    Console.WriteLine("Kérem a szülő SZ_ID-ját: ");
                    string id = Console.ReadLine();
                    Console.WriteLine("Kérem a szülő új települését: ");
                    string uj_település = Console.ReadLine();
                    SzuloTelepulesModositas(szLogic, id, uj_település);
                    Console.WriteLine("-------SZÜLŐ TELEPÜLÉSÉNEK MÓDOSÍTÁSA VÉGE-----------");
                }
                else if (selected == "27")
                {
                    Console.WriteLine("-------SZÜLŐ HÍRLEVÉL MÓDOSÍTÁSA-----------");
                    Console.WriteLine("Kérem a szülő SZ_ID-ját: ");
                    string id = Console.ReadLine();
                    Console.WriteLine("Kérem a szülő új Hírlevél státuszát: ");
                    short uj_hirlevel = short.Parse(Console.ReadLine());
                    SzuloHirlevelModositas(szLogic, id, uj_hirlevel);
                    Console.WriteLine("-------SZÜLŐ HÍRLEVÉL MÓDOSÍTÁSA VÉGE-----------");
                }
                else if (selected == "29")
                {
                    Console.WriteLine("-------SZÜLŐ TÖRLÉSE-----------");
                    Console.WriteLine("Kérem a szülő SZ_ID-ját: ");
                    string id = Console.ReadLine();
                    SzuloTorles(szLogic, id);
                    Console.WriteLine("-------SZÜLŐ TÖRLÉSE VÉGE-----------");
                }
                else if (selected == "31")
                {
                    Console.WriteLine("---------TÁBOROK LISTÁZÁSÁNAK KEZDETE----------");
                    TaborListazas(tLogic);
                    Console.WriteLine("---------TÁBOROK LISTÁZÁSÁNAK VÉGE-------------");
                }
                else if (selected == "311")
                {
                    Console.WriteLine("-------TÁBOR-------");
                    Console.WriteLine("Kérem a Tábor ID-t: ");
                    long id = long.Parse(Console.ReadLine());
                    var tabor = tLogic.GetOne(id);
                    Console.WriteLine(tabor.ToString());
                    Console.WriteLine("-----TÁBOR VÉGE---------");
                }
                else if (selected == "32")
                {
                    Console.WriteLine("--------ÚJ TÁBOR HOZZÁADÁSA------");
                    Console.WriteLine("Kérem a tábor ID-ját, nevét, kezdését, befejezését, szabad helyeit, maximális létszámát és a helyszínt, ahova tartozik(vesszővel elválasztva): ");
                    string[] adatok = Console.ReadLine().Split(',');
                    Taborok ujTabor = new Taborok()
                    {
                        ID = long.Parse(adatok[0]),
                        Nev = adatok[1],
                        Kezdes = DateTime.Parse(adatok[2]),
                        Befejezes = DateTime.Parse(adatok[3]),
                        Szabad_helyek = long.Parse(adatok[4]),
                        Maxfo = long.Parse(adatok[5]),
                        H_ID = long.Parse(adatok[6]),
                    };
                    TaborHozzaadas(tLogic, ujTabor);
                    Console.WriteLine("--------ÚJ TÁBOR HOZZÁADÁSA VÉGE------");
                }
                else if (selected == "33")
                {
                    Console.WriteLine("-------TÁBOR NEVÉNEK MÓDOSÍTÁSA-----------");
                    Console.WriteLine("Kérem a tábor ID-ját: ");
                    long id = long.Parse(Console.ReadLine());
                    Console.WriteLine("Kérem a tábor új nevét: ");
                    string uj_nev = Console.ReadLine();
                    TaborNevModositas(tLogic, id, uj_nev);
                    Console.WriteLine("-------TÁBOR NEVÉNEK MÓDOSÍTÁSA VÉGE-----------");
                }
                else if (selected == "34")
                {
                    Console.WriteLine("-------TÁBOR KEZDÉSÉNEK MÓDOSÍTÁSA-----------");
                    Console.WriteLine("Kérem a tábor ID-ját: ");
                    long id = long.Parse(Console.ReadLine());
                    Console.WriteLine("Kérem a tábor új kezdését: ");
                    DateTime uj_kezdes = DateTime.Parse(Console.ReadLine());
                    TaborKezdesModositas(tLogic, id, uj_kezdes);
                    Console.WriteLine("-------TÁBOR KEZDÉSÉNEK MÓDOSÍTÁSA VÉGE-----------");
                }
                else if (selected == "35")
                {
                    Console.WriteLine("-------TÁBOR BEFEJEZÉSÉNEK MÓDOSÍTÁSA-----------");
                    Console.WriteLine("Kérem a tábor ID-ját: ");
                    long id = long.Parse(Console.ReadLine());
                    Console.WriteLine("Kérem a tábor új befejezését: ");
                    DateTime uj_befejezes = DateTime.Parse(Console.ReadLine());
                    TaborBefejezesModositas(tLogic, id, uj_befejezes);
                    Console.WriteLine("-------TÁBOR BEFEJEZÉSÉNEK MÓDOSÍTÁSA VÉGE-----------");
                }
                else if (selected == "36")
                {
                    Console.WriteLine("-------TÁBOR SZABAD HELYEINEK MÓDOSÍTÁSA-----------");
                    Console.WriteLine("Kérem a tábor ID-ját: ");
                    long id = long.Parse(Console.ReadLine());
                    Console.WriteLine("Kérem a tábor új szabad helyeit: ");
                    long uj_szabad = long.Parse(Console.ReadLine());
                    TaborSzabadHelyModositas(tLogic, id, uj_szabad);
                    Console.WriteLine("-------TÁBOR SZABAD HELYEINEK MÓDOSÍTÁSA VÉGE-----------");
                }
                else if (selected == "37")
                {
                    Console.WriteLine("-------TÁBOR MAXFŐ MÓDOSÍTÁSA-----------");
                    Console.WriteLine("Kérem a tábor ID-ját: ");
                    long id = long.Parse(Console.ReadLine());
                    Console.WriteLine("Kérem a tábor új maxfőjét: ");
                    long uj_maxfo = long.Parse(Console.ReadLine());
                    TaborMaxfoModositas(tLogic, id, uj_maxfo);
                    Console.WriteLine("-------TÁBOR MAXFŐ MÓDOSÍTÁSA VÉGE-----------");
                }
                else if (selected == "39")
                {
                    Console.WriteLine("-------TÁBOR TÖRLÉSE-----------");
                    Console.WriteLine("Kérem a tábor ID-ját: ");
                    long id = long.Parse(Console.ReadLine());
                    TaborTorles(tLogic, id);
                    Console.WriteLine("-------TÁBOR TÖRLÉSE VÉGE-----------");
                }
                else if (selected == "41")
                {
                    Console.WriteLine("---------HELYSZÍNEK LISTÁZÁSÁNAK KEZDETE----------");
                    HelyszinListazas(hLogic);
                    Console.WriteLine("---------HELYSZÍNEK LISTÁZÁSÁNAK VÉGE-------------");
                }
                else if (selected == "411")
                {
                    Console.WriteLine("-------HELYSZÍN-------");
                    Console.WriteLine("Kérem a Helyszín ID-t: ");
                    long id = long.Parse(Console.ReadLine());
                    var helyszin = hLogic.GetOne(id);
                    Console.WriteLine(helyszin.ToString());
                    Console.WriteLine("-----HELYSZÍN VÉGE---------");
                }
                else if (selected == "42")
                {
                    Console.WriteLine("--------ÚJ HELYSZÍN HOZZÁADÁSA------");
                    Console.WriteLine("Kérem a helyszín ID-t, nevét, települését, utcáját, telefonszámát, Maximális befogadóképességét(vesszővel elválasztva): ");
                    string[] adatok = Console.ReadLine().Split(',');
                    Helyszinek ujHelyszin = new Helyszinek()
                    {
                        ID = long.Parse(adatok[0]),
                        Nev = adatok[1],
                        Telepules = adatok[2],
                        Utca = adatok[3],
                        Telefonszam = adatok[4],
                        Maxhely = long.Parse(adatok[5]),
                    };
                    HelyszinHozzaadas(hLogic, ujHelyszin);
                    Console.WriteLine("--------ÚJ HELYSZÍN HOZZÁADÁSA VÉGE------");
                }
                else if (selected == "43")
                {
                    Console.WriteLine("-------HELYSZÍN NEVÉNEK MÓDOSÍTÁSA-----------");
                    Console.WriteLine("Kérem a helyszín ID-ját: ");
                    long id = long.Parse(Console.ReadLine());
                    Console.WriteLine("Kérem a helyszín új nevét: ");
                    string uj_nev = Console.ReadLine();
                    HelyszinNevModositas(hLogic, id, uj_nev);
                    Console.WriteLine("-------HELYSZÍN NEVÉNEK MÓDOSÍTÁSA VÉGE-----------");
                }
                else if (selected == "44")
                {
                    Console.WriteLine("-------HELYSZÍN TELEPÜLÉSÉNEK MÓDOSÍTÁSA-----------");
                    Console.WriteLine("Kérem a helyszín ID-ját: ");
                    long id = long.Parse(Console.ReadLine());
                    Console.WriteLine("Kérem a helyszín új nevét: ");
                    string uj_hely = Console.ReadLine();
                    HelyszinTelepulesModositas(hLogic, id, uj_hely);
                    Console.WriteLine("-------HELYSZÍN TELEPÜLÉSÉNEK MÓDOSÍTÁSA VÉGE-----------");
                }
                else if (selected == "45")
                {
                    Console.WriteLine("-------HELYSZÍN UTCÁJÁNAK MÓDOSÍTÁSA-----------");
                    Console.WriteLine("Kérem a helyszín ID-ját: ");
                    long id = long.Parse(Console.ReadLine());
                    Console.WriteLine("Kérem a helyszín új utcájának nevét: ");
                    string uj_utca = Console.ReadLine();
                    HelyszinUtcaModositas(hLogic, id, uj_utca);
                    Console.WriteLine("-------HELYSZÍN UTCÁJÁNAK MÓDOSÍTÁSA VÉGE-----------");
                }
                else if (selected == "46")
                {
                    Console.WriteLine("-------HELYSZÍN TELEFONSZÁMÁNAK MÓDOSÍTÁSA-----------");
                    Console.WriteLine("Kérem a helyszín ID-ját: ");
                    long id = long.Parse(Console.ReadLine());
                    Console.WriteLine("Kérem a helyszín új telefonszámát: ");
                    string uj_telefon = Console.ReadLine();
                    HelyszinTelefonszamModositas(hLogic, id, uj_telefon);
                    Console.WriteLine("-------HELYSZÍN TELEFONSZÁMÁNAK MÓDOSÍTÁSA VÉGE-----------");
                }
                else if (selected == "47")
                {
                    Console.WriteLine("-------HELYSZÍN MAXHELYÉNEK MÓDOSÍTÁSA-----------");
                    Console.WriteLine("Kérem a helyszín ID-ját: ");
                    long id = long.Parse(Console.ReadLine());
                    Console.WriteLine("Kérem a helyszín új maximális befogadóképességét: ");
                    long uj_maxfo = long.Parse(Console.ReadLine());
                    HelyszinMaxhelyModositas(hLogic, id, uj_maxfo);
                    Console.WriteLine("-------HELYSZÍN MAXHELYÉNEK MÓDOSÍTÁSA VÉGE-----------");
                }
                else if (selected == "48")
                {
                    Console.WriteLine("-------HELYSZÍN TÖRLÉSE-----------");
                    Console.WriteLine("Kérem a helyszín ID-ját: ");
                    long id = long.Parse(Console.ReadLine());
                    HelyszinTorles(hLogic, id);
                    Console.WriteLine("-------HELYSZÍN TÖRLÉSE VÉGE-----------");
                }
                else if (selected == "51")
                {
                    Console.WriteLine("-----HÍRLEVELES SZÜLŐK--------");
                    var hirlevelesSzuloLista = logic.HirlevelesSzulo();
                    foreach (var szulo in hirlevelesSzuloLista)
                    {
                        Console.WriteLine("Név: " + szulo.Szulo);
                    }

                    Console.WriteLine("-----HÍRLEVELES SZÜLŐK VÉGE--------");
                }
                else if (selected == "52")
                {
                    Console.WriteLine("-----TÁBORTÍPUSOK ÁTLAGOS MAXFŐJE--------");
                    var lista = logic.AtlagMaxfoTaborTipusonkent();
                    foreach (var item in lista)
                    {
                        Console.WriteLine("Név: " + item.Tabor + ", Átlag: " + item.AtlagMaxfo);
                    }

                    Console.WriteLine("-----TÁBORTÍPUSOK ÁTLAGOS MAXFŐJE VÉGE--------");
                }
                else if (selected == "53")
                {
                    Console.WriteLine("-----TÁBOROK A HELYSZÍNEKEN--------");
                    var lista = logic.TaborokEsHelyszinek();
                    foreach (var item in lista)
                    {
                        Console.WriteLine("Tábor: " + item.Tabor + ", Helyszín: " + item.Helyszin);
                    }

                    Console.WriteLine("-----TÁBOROK A HELYSZÍNEKEN VÉGE--------");
                }
                else if (selected == "60")
                {
                    Console.WriteLine("Mi legyen az első gyerek neve?");
                    string nev1 = Console.ReadLine();
                    Console.WriteLine("Mi legyen a második gyerek neve?");
                    string nev2 = Console.ReadLine();
                    Console.WriteLine("Mi legyen a harmadik gyerek neve?");
                    string nev3 = Console.ReadLine();
                    Console.WriteLine("Melyik szülőhöz tartozik a gyerek?");
                    string szid = Console.ReadLine();
                    IQueryable<string> szulok = szLogic.Listazas().Select(x => x.SZ_ID);
                    if (!szulok.Contains(szid))
                    {
                        Console.WriteLine("Nincs ilyen szülő! Próbáld újra.");
                    }
                    else
                    {
                        XDocument xDoc = XDocument.Load("http://localhost:8080/FelevesServlet/Gyerek?nev1=" + nev1 + "&nev2=" + nev2 + "&nev3=" + nev3 + "&szId=" + szid);
                        Console.WriteLine("A gyerekeid generált adatokkal:");

                        var lista = (from x in xDoc.Descendants("gyerek")
                                    select new
                                    {
                                        Nev = x.Element("nev").Value,
                                        MC_nev = x.Element("mc").Value,
                                        SzuloID = x.Element("szuloID").Value,
                                        Kor = x.Element("kor"),
                                    }).ToList();

                        foreach (var gyerek in lista)
                        {
                            Console.WriteLine("Gyerek - NEV: " + gyerek.Nev + " ; MC_nev: " + gyerek.MC_nev + " ; Kor: " + gyerek.Kor + " ; SzuloID: " + gyerek.SzuloID);
                        }

                        // Teljes XML dokumentum kiiratása:
                        // Console.WriteLine(xDoc.ToString());
                    }
                }

                selected = Console.ReadLine();
            }
        }

        // SZÜLŐ METÓDUSOK KEZDETE
        private static void SzuloListazas(SzuloLogic szLogic)
        {
            var lista = szLogic.Listazas();
            foreach (var item in lista)
            {
                Console.WriteLine(item.ToString());
            }
        }

        private static void SzuloHozzaadas(SzuloLogic szLogic, Szulok ujSzulo)
        {
            szLogic.Hozzaadas(ujSzulo);
            Console.WriteLine("Szülő hozzáadása megtörtént.");
        }

        private static void SzuloNevModositas(SzuloLogic szLogic, string id, string uj_nev)
        {
            szLogic.Nev_Modositas(id, uj_nev);
            Console.WriteLine("A szülő nevének módosítása megtörtént.");
        }

        private static void SzuloTelefonModositas(SzuloLogic szLogic, string id, string uj_telefon)
        {
            szLogic.Telefonszam_Modositas(id, uj_telefon);
            Console.WriteLine("A szülő telefonszámának módosítása megtörtént.");
        }

        private static void SzuloEmailModositas(SzuloLogic szLogic, string id, string uj_email)
        {
            szLogic.Email_Modositas(id, uj_email);
            Console.WriteLine("A szülő e-mail címének módosítása megtörtént.");
        }

        private static void SzuloTelepulesModositas(SzuloLogic szLogic, string id, string uj_telepules)
        {
            szLogic.Telepules_Modositas(id, uj_telepules);
            Console.WriteLine("A szülő településének módosítása megtörtént.");
        }

        private static void SzuloHirlevelModositas(SzuloLogic szLogic, string id, short uj_hirlevel)
        {
            szLogic.Hirlevel_Modositas(id, uj_hirlevel);
            Console.WriteLine("A szülő hírlevél státuszának módosítása megtörtént.");
        }

        private static void SzuloIdModositas(SzuloLogic szLogic, string id, string uj_id)
        {
            szLogic.ID_Modositas(id, uj_id);
            Console.WriteLine("A szülő személyi igazolványának(PK) módosítása megtörtént.");
        }

        private static void SzuloTorles(SzuloLogic szLogic, string id)
        {
            szLogic.Torles(id);
            Console.WriteLine("A szülő törlése megtörtént.");
        }

        // SZÜLŐ METÓDUSOK VÉGE

        // GYEREK METÓDUSOK KEZDETE
        private static void GyerekListazas(GyerekLogic gyLogic)
        {
            var lista = gyLogic.Listazas();
            foreach (var item in lista)
            {
                Console.WriteLine(item.ToString());
            }
        }

        private static void GyerekHozzaadas(GyerekLogic gyLogic, Gyerekek ujGyerek)
        {
            gyLogic.Hozzaadas(ujGyerek);
            Console.WriteLine("A gyerek hozzáadása megtörtént.");
        }

        private static void GyerekIdModositas(GyerekLogic gyLogic, string id, string uj_id)
        {
            gyLogic.MC_Modositas(id, uj_id);
            Console.WriteLine("A gyerek MC felhasználómevének megváltoztatása megtörtént. (id)");
        }

        private static void GyerekNevModositas(GyerekLogic gyLogic, string id, string uj_nev)
        {
            gyLogic.Nev_Modositas(id, uj_nev);
            Console.WriteLine("A gyerek nevének módosítása megtörtént.");
        }

        private static void GyerekKorModositas(GyerekLogic gyLogic, string id, string uj_kor)
        {
            gyLogic.Kor_Modositas(id, uj_kor);
            Console.WriteLine("A gyerek korának módosítása megtörtént.");
        }

        private static void GyerekSzuloModositas(GyerekLogic gyLogic, string id, string uj_SZ)
        {
            gyLogic.SZ_Modositas(id, uj_SZ);
            Console.WriteLine("A gyerek szülőjének módosítása megtörtént.(FK)");
        }

        private static void GyerekTorles(GyerekLogic gyLogic, string id)
        {
            gyLogic.Torles(id);
            Console.WriteLine("A gyerek törlése megtörtént.");
        }

        // GYEREK METÓDUSOK VÉGE

        // TÁBOR METÓDUSOK KEZDETE
        private static void TaborListazas(TaborLogic tLogic)
        {
            var lista = tLogic.Listazas();
            foreach (var item in lista)
            {
                Console.WriteLine(item.ToString());
            }
        }

        private static void TaborHozzaadas(TaborLogic tLogic, Taborok ujTabor)
        {
            tLogic.Hozzaadas(ujTabor);
            Console.WriteLine("A tábor hozzáadása megtörtént.");
        }

        private static void TaborNevModositas(TaborLogic tLogic, long id, string uj_nev)
        {
            tLogic.Nev_Modositas(id, uj_nev);
            Console.WriteLine("A tábor nevének módosítása megtörtént.");
        }

        private static void TaborKezdesModositas(TaborLogic tLogic, long id, DateTime uj_kezdes)
        {
            tLogic.Kezdes_Modositas(id, uj_kezdes);
            Console.WriteLine("A tábor kezdési időpontjának módosítása megtörtént.");
        }

        private static void TaborBefejezesModositas(TaborLogic tLogic, long id, DateTime uj_befejezes)
        {
            tLogic.Befejezes_Modositas(id, uj_befejezes);
            Console.WriteLine("A tábor befejezési időpontjának módosítása megtörtént.");
        }

        private static void TaborSzabadHelyModositas(TaborLogic tLogic, long id, long uj_szabadHely)
        {
            tLogic.SzabadHelyek_Modositas(id, uj_szabadHely);
            Console.WriteLine("A tábor szabad helyeinek módosítása megtörtént.");
        }

        private static void TaborMaxfoModositas(TaborLogic tLogic, long id, long uj_maxfo)
        {
            tLogic.Maxfo_Modositas(id, uj_maxfo);
            Console.WriteLine("A tábor maximális létszámának módosítása megtörtént.");
        }

        private static void TaborHelyIdModositas(TaborLogic tLogic, long id, long uj_hID)
        {
            tLogic.HID_Modositas(id, uj_hID);
            Console.WriteLine("A tábor helyszínének(ID) módosítása megtörtént.");
        }

        private static void TaborTorles(TaborLogic tLogic, long id)
        {
            tLogic.Torles(id);
            Console.WriteLine("A tábor törlése megtörtént.");
        }

        // TÁBOR METÓDUSOK VÉGE

        // HELYSZÍN METÓDUSOK KEZDETE
        private static void HelyszinListazas(HelyszinLogic hLogic)
        {
            var lista = hLogic.Listazas();
            foreach (var item in lista)
            {
                Console.WriteLine(item.ToString());
            }
        }

        private static void HelyszinHozzaadas(HelyszinLogic hLogic, Helyszinek ujHelyszin)
        {
            hLogic.Hozzaadas(ujHelyszin);
            Console.WriteLine("A helyszín hozzáadása megtörtént.");
        }

        private static void HelyszinNevModositas(HelyszinLogic hLogic, long id, string uj_nev)
        {
            hLogic.Nev_Modositas(id, uj_nev);
            Console.WriteLine("A helyszín nevének módosítása megtörtént.");
        }

        private static void HelyszinTelepulesModositas(HelyszinLogic hLogic, long id, string uj_telepules)
        {
            hLogic.Telepules_Modositas(id, uj_telepules);
            Console.WriteLine("A helyszín településének módosítása megtörtént.");
        }

        private static void HelyszinUtcaModositas(HelyszinLogic hLogic, long id, string uj_utca)
        {
            hLogic.Utca_Modositas(id, uj_utca);
            Console.WriteLine("A helyszín utcájának módosítása megtörtént");
        }

        private static void HelyszinTelefonszamModositas(HelyszinLogic hlogic, long id, string uj_telefon)
        {
            hlogic.Telefonszam_Modositas(id, uj_telefon);
        }

        private static void HelyszinMaxhelyModositas(HelyszinLogic hLogic, long id, long uj_maxhely)
        {
            hLogic.Maxhely_Modositas(id, uj_maxhely);
            Console.WriteLine("A helyszín maximális befogadóképességének módosítása megtörtént.");
        }

        private static void HelyszinTorles(HelyszinLogic hLogic, long id)
        {
            hLogic.Torles(id);
            Console.WriteLine("A helyszín törlése megtörtént.");
        }
    }
}
