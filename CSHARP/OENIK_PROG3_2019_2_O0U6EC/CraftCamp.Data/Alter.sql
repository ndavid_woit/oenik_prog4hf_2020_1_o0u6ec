﻿DROP TABLE Jelentkezesek;
DROP TABLE Taborok;
DROP TABLE Helyszinek;

CREATE TABLE Helyszinek(
    ID bigint IDENTITY PRIMARY KEY NOT NULL,
    Nev varchar(50),
    Telepules varchar(50),
    Utca varchar(50),
    Telefonszam varchar(50),
    Maxhely bigint
);

CREATE TABLE Taborok(
    ID bigint primary key NOT NULL,
    Nev varchar(50) NOT NULL,
    Kezdes Datetime,
    Befejezes Datetime,
    Szabad_helyek bigint,
    Maxfo bigint,
    H_ID bigint REFERENCES Helyszinek (ID)
);

CREATE TABLE Jelentkezesek(
    ID bigint primary key,
    GY_ID varchar(50) REFERENCES Gyerekek (MC_USER),
    T_ID bigint REFERENCES Taborok (ID),
    Kuponkod varchar(50),
    Megjegyzes varchar(300),
    Datum Date
);

INSERT INTO Helyszinek (Nev, Telepules, Utca, Telefonszam, Maxhely) VALUES ('FunnyHouse Közösségi Ház', 'Debrecen','Csapókert', '06304477556', 30); 
INSERT INTO Helyszinek (Nev, Telepules, Utca, Telefonszam, Maxhely) VALUES ('Entranet Alapítvány', 'Nyíregyháza', 'Fő', '06701123647', 25); 
INSERT INTO Helyszinek (Nev, Telepules, Utca, Telefonszam, Maxhely) VALUES ('ÚjNemzedék közösségi tér', 'Budapest', 'Kossuth', '06204478852',45); 
INSERT INTO Helyszinek (Nev, Telepules, Utca, Telefonszam, Maxhely) VALUES ('Városi Művelődési központ', 'Pécs', 'Széchenyi', '06704415978', 17); 
INSERT INTO Helyszinek (Nev, Telepules, Utca, Telefonszam, Maxhely) VALUES ('Megyei Könyvtár', 'Nyíregyháza', 'Városmajor', '06302247851', 50); 
INSERT INTO Taborok VALUES(1, 'History', '2019.07.01','2019.07.08', 16, 16, 2); 
INSERT INTO Taborok VALUES(2, 'Halloween', '2019.10.28', '2019.10.31', 13, 13, 5); 
INSERT INTO Taborok VALUES(3, 'SummerTime','2019.08.10', '2019.08.16', 13, 13, 1); 
INSERT INTO Taborok VALUES(4, 'History','2019.07.12','2019.07.19', 15, 15, 3); 
INSERT INTO Taborok VALUES(5, 'Öko', '2019.07.20','2019.07.25', 10, 10, 3); 
INSERT INTO Jelentkezesek VALUES (1, 'ShadoWarrior', 2,null,'A gyermekem lisztérzékeny','2019.10.01'); 
INSERT INTO Jelentkezesek VALUES (2, 'Camembert12', 2,null,'Az első nap késni fog!','2019.09.15'); 
INSERT INTO Jelentkezesek VALUES (3, 'Okoska25', 2,null,null,'2019.10.25'); 
INSERT INTO Jelentkezesek VALUES (4, 'Okoska25', 3,'CCA2019','Nem kér ebédet, hanem a saját ebédjét eszi meg!','2019.08.01'); 
INSERT INTO Jelentkezesek VALUES (5, 'Paradicsom69', 4,null,null,'2019.06.26'); 
INSERT INTO Jelentkezesek VALUES (6, 'Királylány', 3,null,'Csak laktózmentes ételt eszik!', '2019.03.24'); 

SET IDENTITY_INSERT Helyszinek ON;
