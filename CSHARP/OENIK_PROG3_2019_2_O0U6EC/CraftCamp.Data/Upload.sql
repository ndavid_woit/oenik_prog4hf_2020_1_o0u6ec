﻿CREATE TABLE Szulok(
    SZ_ID VARCHAR(50) PRIMARY KEY not null,
    Nev varchar(50) NOT NULL,
    Telefonszam varchar(50),
    Email varchar(50) UNIQUE,
    Telepules varchar(50),
    Hirlevel SMALLINT,
    CONSTRAINT email_ch CHECK(Email LIKE '%@%' AND EMAIL LIKE '%.%')
);

CREATE TABLE Gyerekek(
    MC_USER VARCHAR(50) PRIMARY KEY not null,
    Nev varchar(50) NOT NULL,
    Kor varchar(50),
    SZ_ID varchar(50) REFERENCES Szulok(SZ_ID)
);

CREATE TABLE Helyszinek(
    ID bigint PRIMARY KEY NOT NULL,
    Nev varchar(50),
    Telepules varchar(50),
    Utca varchar(50),
    Telefonszam varchar(50),
    Maxhely bigint
);

CREATE TABLE Taborok(
    ID bigint primary key NOT NULL,
    Nev varchar(50) NOT NULL,
    Kezdes Datetime,
    Befejezes Datetime,
    Szabad_helyek bigint,
    Maxfo bigint,
    H_ID bigint REFERENCES Helyszinek (ID)
);

CREATE TABLE Jelentkezesek(
    ID bigint primary key,
    GY_ID varchar(50) REFERENCES Gyerekek (MC_USER),
    T_ID bigint(10) REFERENCES Taborok (ID),
    Kuponkod varchar(50),
    Megjegyzes varchar(300),
    Datum Date
);