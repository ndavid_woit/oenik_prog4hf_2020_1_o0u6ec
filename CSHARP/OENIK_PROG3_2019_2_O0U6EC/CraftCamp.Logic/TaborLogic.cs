﻿// <copyright file="TaborLogic.cs" company="Nagy Dávid O0U6EC">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CraftCamp.Logic
{
    using System;
    using System.Linq;
    using CraftCamp.Data;
    using CraftCamp.Repository;

    /// <summary>
    /// TaborLogic.
    /// </summary>
    public class TaborLogic : ITaborLogic
    {
        private ITaborRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="TaborLogic"/> class.
        /// </summary>
        /// <param name="repo">TaborRepo.</param>
        public TaborLogic(ITaborRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TaborLogic"/> class.
        /// </summary>
        public TaborLogic()
        {
            this.repo = new TaborRepository(new CraftCampDBEntities());
        }

        /// <summary>
        /// UPDATE BEFEJEZES TO uj_befejezes.
        /// </summary>
        /// <param name="id">TaborID.</param>
        /// <param name="uj_befejezes">Új befejezés.</param>
        public void Befejezes_Modositas(long id, DateTime uj_befejezes)
        {
            this.repo.Befejezes_Modositas(id, uj_befejezes);
        }

        /// <summary>
        /// UPDATE HelyszínID to uj_hID.
        /// </summary>
        /// <param name="id">TáborID.</param>
        /// <param name="uj_hID">Új helyszínID.</param>
        public void HID_Modositas(long id, long uj_hID)
        {
            this.repo.HID_Modositas(id, uj_hID);
        }

        /// <summary>
        /// INSERT ujTabor.
        /// </summary>
        /// <param name="ujTabor">Új Tábor.</param>
        public void Hozzaadas(Taborok ujTabor)
        {
            this.repo.Hozzaadas(ujTabor);
        }

        /// <summary>
        /// UPDATE ID TO uj_id.
        /// </summary>
        /// <param name="id">TáborID.</param>
        /// <param name="uj_id">Új TáborID.</param>
        public void ID_Modositas(long id, long uj_id)
        {
            this.repo.ID_Modositas(id, uj_id);
        }

        /// <summary>
        /// UPDATE KEZDES TO uj_kezdes.
        /// </summary>
        /// <param name="id">TáborID.</param>
        /// <param name="uj_kezdes">Új kezdés.</param>
        public void Kezdes_Modositas(long id, DateTime uj_kezdes)
        {
            this.repo.Kezdes_Modositas(id, uj_kezdes);
        }

        /// <summary>
        /// SELECT ALL.
        /// </summary>
        /// <returns>IQueryable Taborok type.</returns>
        public IQueryable<Taborok> Listazas()
        {
            return this.repo.Listazas();
        }

        /// <summary>
        /// UPDATE MAXFO TO uj_maxfo.
        /// </summary>
        /// <param name="id">TáborID.</param>
        /// <param name="uj_maxfo">Új maxfő.</param>
        public void Maxfo_Modositas(long id, long uj_maxfo)
        {
            this.repo.Maxfo_Modositas(id, uj_maxfo);
        }

        /// <summary>
        /// UPDATE NEV TO uj_nev.
        /// </summary>
        /// <param name="id">TáborID.</param>
        /// <param name="uj_nev">Új név.</param>
        public void Nev_Modositas(long id, string uj_nev)
        {
            this.repo.Nev_Modositas(id, uj_nev);
        }

        /// <summary>
        /// UPDATE SZABADHELYEK TO uj_szabadHelyek.
        /// </summary>
        /// <param name="id">TáborID.</param>
        /// <param name="uj_szabadHelyek">Új szabadhelyek.</param>
        public void SzabadHelyek_Modositas(long id, long uj_szabadHelyek)
        {
            this.repo.SzabadHelyek_Modositas(id, uj_szabadHelyek);
        }

        /// <summary>
        /// DELETE BY ID.
        /// </summary>
        /// <param name="id">TáborID.</param>
        public void Torles(long id)
        {
            this.repo.Torles(id);
        }

        /// <summary>
        /// SELECT BY ID.
        /// </summary>
        /// <param name="id">TáborID.</param>
        /// <returns>A kiválasztott ID-jű Tábor.</returns>
        public Taborok GetOne(long id)
        {
            return this.repo.GetOne(id);
        }
    }
}
