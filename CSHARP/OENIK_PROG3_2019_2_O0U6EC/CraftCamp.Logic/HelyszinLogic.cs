﻿// <copyright file="HelyszinLogic.cs" company="Nagy Dávid O0U6EC">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CraftCamp.Logic
{
    using System.Linq;
    using CraftCamp.Data;
    using CraftCamp.Repository;

    /// <summary>
    /// HelyszinLogic.
    /// </summary>
    public class HelyszinLogic : IHelyszinLogic
    {
        private IHelyszinRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="HelyszinLogic"/> class.
        /// </summary>
        /// <param name="repo">HelyszinRepo.</param>
        public HelyszinLogic(IHelyszinRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HelyszinLogic"/> class.
        /// </summary>
        public HelyszinLogic()
        {
            this.repo = new HelyszinRepository(new CraftCampDBEntities());
        }

        /// <summary>
        /// INSERT ujHelyszin.
        /// </summary>
        /// <param name="ujHelyszin">Új helyszín.</param>
        public void Hozzaadas(Helyszinek ujHelyszin)
        {
            Helyszinek h = new Helyszinek();
            h.Maxhely = ujHelyszin.Maxhely;
            h.Nev = ujHelyszin.Nev;
            h.Telefonszam = ujHelyszin.Telefonszam;
            h.Telepules = ujHelyszin.Telepules;
            h.Utca = ujHelyszin.Utca;
            this.repo.Hozzaadas(h);
        }

        /// <summary>
        /// UPDATE ID TO uj_id.
        /// </summary>
        /// <param name="id">HelyszínID.</param>
        /// <param name="uj_id">Új helyszínID.</param>
        public void ID_Modositas(long id, long uj_id)
        {
            this.repo.ID_Modositas(id, uj_id);
        }

        /// <summary>
        /// SELECT ALL HELYSZINEK.
        /// </summary>
        /// <returns>IQueryable Helyszínek type.</returns>
        public IQueryable<Helyszinek> Listazas()
        {
            return this.repo.Listazas();
        }

        /// <summary>
        /// UPDATE MAXHELY TO uj_maxhely.
        /// </summary>
        /// <param name="id">HelyszínID.</param>
        /// <param name="uj_maxhely">Új maxhely.</param>
        public void Maxhely_Modositas(long id, long uj_maxhely)
        {
            this.repo.Maxhely_Modositas(id, uj_maxhely);
        }

        /// <summary>
        /// UPDATE NEV to uj_nev.
        /// </summary>
        /// <param name="id">HelyszínID.</param>
        /// <param name="uj_nev">Új név.</param>
        public void Nev_Modositas(long id, string uj_nev)
        {
            this.repo.Nev_Modositas(id, uj_nev);
        }

        /// <summary>
        /// UPDATE TELEFONSZAM TO uj_telefon.
        /// </summary>
        /// <param name="id">HelyszínID.</param>
        /// <param name="uj_telefon">Új telefonszám.</param>
        public void Telefonszam_Modositas(long id, string uj_telefon)
        {
            this.repo.Telefonszam_Modositas(id, uj_telefon);
        }

        /// <summary>
        /// UPDATE TELEPULES TO uj_telepules.
        /// </summary>
        /// <param name="id">HelyszínID.</param>
        /// <param name="uj_telepules">Új település.</param>
        public void Telepules_Modositas(long id, string uj_telepules)
        {
            this.repo.Telepules_Modositas(id, uj_telepules);
        }

        /// <summary>
        /// DELETE HELYSZÍN BY ID.
        /// </summary>
        /// <param name="id">HelyszínID.</param>
        /// <returns>TRUE/FALSE based on success or failure.</returns>
        public bool Torles(long id)
        {
            return this.repo.Torles(id);
        }

        /// <summary>
        /// UPDATE UTCA TO uj_utca.
        /// </summary>
        /// <param name="id">HelyszínID.</param>
        /// <param name="uj_utca">Új utca.</param>
        public void Utca_Modositas(long id, string uj_utca)
        {
            this.repo.Utca_Modositas(id, uj_utca);
        }

        /// <summary>
        /// SELECT HELYSZÍN BY ID.
        /// </summary>
        /// <param name="id">HelyszínID.</param>
        /// <returns>Az adott ID-jű Helyszín.</returns>
        public Helyszinek GetOne(long id)
        {
            return this.repo.GetOne(id);
        }
    }
}
