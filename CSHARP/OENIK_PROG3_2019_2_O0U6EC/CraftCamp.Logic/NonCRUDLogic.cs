﻿// <copyright file="NonCRUDLogic.cs" company="Nagy Dávid O0U6EC">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CraftCamp.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using CraftCamp.Data;
    using CraftCamp.Logic.NonCrudClasses;
    using CraftCamp.Repository;

    /// <summary>
    /// nonCrudLogic.
    /// </summary>
    public class NonCRUDLogic : InonCRUDLOGIC
    {
        private ITaborRepository tRepo;
        private ISzuloRepository szRepo;
        private IHelyszinRepository hRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="NonCRUDLogic"/> class.
        /// </summary>
        /// <param name="szRepo">SzuloRepo.</param>
        public NonCRUDLogic(ISzuloRepository szRepo)
        {
            this.szRepo = szRepo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NonCRUDLogic"/> class.
        /// </summary>
        /// <param name="tRepo">TaborRepo.</param>
        public NonCRUDLogic(ITaborRepository tRepo)
        {
            this.tRepo = tRepo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NonCRUDLogic"/> class.
        /// </summary>
        /// <param name="hRepo">HelyszinRepo.</param>
        public NonCRUDLogic(IHelyszinRepository hRepo)
        {
            this.hRepo = hRepo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NonCRUDLogic"/> class.
        /// </summary>
        /// <param name="hRepo">HelyszinRepo.</param>
        /// <param name="tRepo">TaborRepo.</param>
        public NonCRUDLogic(IHelyszinRepository hRepo, ITaborRepository tRepo)
        {
            this.hRepo = hRepo;
            this.tRepo = tRepo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NonCRUDLogic"/> class.
        /// </summary>
        public NonCRUDLogic()
        {
            CraftCampDBEntities ctx = new CraftCampDBEntities();
            this.tRepo = new TaborRepository(ctx);
            this.szRepo = new SzuloRepository(ctx);
            this.hRepo = new HelyszinRepository(ctx);
        }

        /// <summary>
        /// Kiválasztja azokat a szülőket, akik felvannak iratkozva hírlevélre.
        /// </summary>
        /// <returns>Lista, amely 'HirlevelesSzulok'-et tartalmaz.</returns>
        public List<HirlevelesSzulo> HirlevelesSzulo()
        {
            var q = from x in this.szRepo.Listazas()
                    where x.Hirlevel.Value.ToString() == "1"
                    orderby x.Nev
                    select new HirlevelesSzulo { Szulo = x.Nev, SZ_ID = x.SZ_ID, Email = x.Email, Hirlevel = x.Hirlevel.Value };

            return q.ToList();
        }

        /// <summary>
        /// Megkeresi a Tábortípusokat és kiírja, hogy az egyes típusokat átlag milyen maxlétszámmal indították.
        /// </summary>
        /// <returns>Lista, amely a tábortípusokat és a hozzájuk tartozó maxlétszám átlagát tartalmazza.</returns>
        public List<TaborAtlagMaxfo> AtlagMaxfoTaborTipusonkent()
        {
            var q = from x in this.tRepo.Listazas()
                    group x by x.Nev into g
                    select new TaborAtlagMaxfo { Tabor = g.Key, AtlagMaxfo = g.Average(x => x.Maxfo.Value) };

            return q.ToList();
        }

        /// <summary>
        /// Kiválasztja, hogy az egyes táborok mely helyszíneken kerülnek megrendezésre.
        /// </summary>
        /// <returns>Lista, amely a táborokat és a hozzá tartozó helyszíneket tartalmazza.</returns>
        public List<TaborokHelyszinek> TaborokEsHelyszinek()
        {
            var taborok = this.tRepo.Listazas();
            var helyszinek = this.hRepo.Listazas();
            var result = from x in taborok
                         join k in helyszinek on x.H_ID equals k.ID
                         select new TaborokHelyszinek { Tabor = x.Nev, Helyszin = k.Nev };

            return result.ToList();
        }
    }
}
