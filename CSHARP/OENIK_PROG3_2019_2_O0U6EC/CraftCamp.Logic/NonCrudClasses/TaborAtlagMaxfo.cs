﻿// <copyright file="TaborAtlagMaxfo.cs" company="Nagy Dávid O0U6EC">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CraftCamp.Logic.NonCrudClasses
{
    using System;

    /// <summary>
    /// Tábor átlagos maxlétszámának az osztálya.
    /// </summary>
    public class TaborAtlagMaxfo
    {
        /// <summary>
        /// Gets or Sets Tábor neve.
        /// </summary>
        public string Tabor { get; set; }

        /// <summary>
        /// Gets or Sets Átlagos Maxfő.
        /// </summary>
        public double AtlagMaxfo { get; set; }

        /// <summary>
        /// Overrided Equals.
        /// </summary>
        /// <param name="obj">'TaborAtlagMaxfo' típust várunk.</param>
        /// <returns>Megegyezik-e? (True/False).</returns>
        public override bool Equals(object obj)
        {
            if (obj is TaborAtlagMaxfo)
            {
                TaborAtlagMaxfo other = obj as TaborAtlagMaxfo;
                return this.Tabor == other.Tabor && Math.Abs(this.AtlagMaxfo - other.AtlagMaxfo) < 0.001;
            }

            return false;
        }

        /// <summary>
        /// Overrided GetHashCode.
        /// </summary>
        /// <returns>HashCode.</returns>
        public override int GetHashCode()
        {
            return 0;
        }
    }
}
