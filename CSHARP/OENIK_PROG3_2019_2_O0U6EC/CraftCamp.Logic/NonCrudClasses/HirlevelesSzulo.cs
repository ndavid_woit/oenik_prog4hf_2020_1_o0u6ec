﻿// <copyright file="HirlevelesSzulo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CraftCamp.Logic.NonCrudClasses
{
    /// <summary>
    /// A Hírleveles Szülők osztálya.
    /// </summary>
    public class HirlevelesSzulo
    {
        /// <summary>
        /// Gets or Sets Szulo nev.
        /// </summary>
        public string Szulo { get; set; }

        /// <summary>
        /// Gets or Sets Szülő ID-jét.
        /// </summary>
        public string SZ_ID { get; set; }

        /// <summary>
        /// Gets or Sets Szülő Emailjét.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or Sets Hirlevel státusz.
        /// </summary>
        public short Hirlevel { get; set; }

        /// <summary>
        /// Overrided Equals.
        /// </summary>
        /// <param name="obj">'HirlevelesSzulo' típust várunk.</param>
        /// <returns>Egyenlő-e? (True/False).</returns>
        public override bool Equals(object obj)
        {
            if (obj is HirlevelesSzulo)
            {
                HirlevelesSzulo other = obj as HirlevelesSzulo;
                return this.SZ_ID == other.SZ_ID;
            }

            return false;
        }

        /// <summary>
        /// Overrided GetHashCode.
        /// </summary>
        /// <returns>HashCode.</returns>
        public override int GetHashCode()
        {
            return 0;
        }
    }
}
