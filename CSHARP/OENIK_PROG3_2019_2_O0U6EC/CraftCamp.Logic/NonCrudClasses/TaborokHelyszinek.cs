﻿// <copyright file="TaborokHelyszinek.cs" company="Nagy Dávid O0U6EC">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CraftCamp.Logic.NonCrudClasses
{
    /// <summary>
    /// Táborok és a hozzájuk tartozó helyszínek osztálya.
    /// </summary>
    public class TaborokHelyszinek
    {
        /// <summary>
        /// Gets or Sets Tábor neve.
        /// </summary>
        public string Tabor { get; set; }

        /// <summary>
        /// Gets or Sets Helyszín neve.
        /// </summary>
        public string Helyszin { get; set; }

        /// <summary>
        /// Overrided Equals.
        /// </summary>
        /// <param name="obj">'TaborokHelyszinek' típust várunk.</param>
        /// <returns>Megegyezik-e? (True/False).</returns>
        public override bool Equals(object obj)
        {
            if (obj is TaborokHelyszinek)
            {
                TaborokHelyszinek other = obj as TaborokHelyszinek;
                return this.Tabor == other.Tabor && this.Helyszin == other.Helyszin;
            }

            return false;
        }

        /// <summary>
        /// Overrided GetHashCode.
        /// </summary>
        /// <returns>HashCode.</returns>
        public override int GetHashCode()
        {
            return 0;
        }
    }
}
