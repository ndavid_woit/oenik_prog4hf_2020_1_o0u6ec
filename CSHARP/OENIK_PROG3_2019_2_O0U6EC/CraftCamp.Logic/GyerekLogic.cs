﻿// <copyright file="GyerekLogic.cs" company="Nagy Dávid O0U6EC">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CraftCamp.Logic
{
    using System.Linq;
    using CraftCamp.Data;
    using CraftCamp.Repository;

    /// <summary>
    /// GyerekLogic.
    /// </summary>
    public class GyerekLogic : IGyerekLogic
    {
        private IGyerekRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="GyerekLogic"/> class.
        /// </summary>
        /// <param name="repo">GyerekRepo.</param>
        public GyerekLogic(IGyerekRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GyerekLogic"/> class.
        /// </summary>
        public GyerekLogic()
        {
            this.repo = new GyerekRepository(new CraftCampDBEntities());
        }

        /// <summary>
        /// INSERT ujGyerek.
        /// </summary>
        /// <param name="ujGyerek">Gyerek_ID.</param>
        public void Hozzaadas(Gyerekek ujGyerek)
        {
            this.repo.Hozzaadas(ujGyerek);
        }

        /// <summary>
        /// UPDATE KOR TO uj_kor.
        /// </summary>
        /// <param name="id">Gyerek_ID.</param>
        /// <param name="uj_kor">Új kor.</param>
        public void Kor_Modositas(string id, string uj_kor)
        {
            this.repo.Kor_Modositas(id, uj_kor);
        }

        /// <summary>
        /// SELECT ALL.
        /// </summary>
        /// <returns>IQueryable Gyerek type.</returns>
        public IQueryable<Gyerekek> Listazas()
        {
            return this.repo.Listazas();
        }

        /// <summary>
        /// UPDATE ID TO uj_id.
        /// </summary>
        /// <param name="id">Gyerek_ID.</param>
        /// <param name="uj_id">Gyerek új ID.</param>
        public void MC_Modositas(string id, string uj_id)
        {
            this.repo.MC_Modositas(id, uj_id);
        }

        /// <summary>
        /// UPDATE NEV TO uj_nev.
        /// </summary>
        /// <param name="id">Gyerek_ID.</param>
        /// <param name="uj_nev">Új név.</param>
        public void Nev_Modositas(string id, string uj_nev)
        {
            this.repo.Nev_Modositas(id, uj_nev);
        }

        /// <summary>
        /// UPDATE SZ_ID to uj_SZ.
        /// </summary>
        /// <param name="id">Gyerek_ID.</param>
        /// <param name="uj_SZ">Új SZ_ID.</param>
        public void SZ_Modositas(string id, string uj_SZ)
        {
            this.repo.SZ_Modositas(id, uj_SZ);
        }

        /// <summary>
        /// DELETE BY ID.
        /// </summary>
        /// <param name="id">GyerekID.</param>
        public void Torles(string id)
        {
            this.repo.Torles(id);
        }

        /// <summary>
        /// SELECT BY ID.
        /// </summary>
        /// <param name="id">GyerekID.</param>
        /// <returns>Az adott ID-jű GYEREK.</returns>
        public Gyerekek GetOne(string id)
        {
            return this.repo.GetOne(id);
        }
    }
}
