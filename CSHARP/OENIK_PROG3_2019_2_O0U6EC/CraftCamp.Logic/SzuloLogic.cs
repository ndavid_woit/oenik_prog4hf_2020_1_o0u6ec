﻿// <copyright file="SzuloLogic.cs" company="Nagy Dávid O0U6EC">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CraftCamp.Logic
{
    using System.Linq;
    using CraftCamp.Data;
    using CraftCamp.Repository;

    /// <summary>
    /// SzuloLogic.
    /// </summary>
    public class SzuloLogic : ISzuloLogic
    {
        private ISzuloRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="SzuloLogic"/> class.
        /// </summary>
        /// <param name="repo">SzuloRepo.</param>
        public SzuloLogic(ISzuloRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SzuloLogic"/> class.
        /// </summary>
        public SzuloLogic()
        {
            this.repo = new SzuloRepository(new CraftCampDBEntities());
        }

        /// <summary>
        /// SELECT ALL.
        /// </summary>
        /// <returns>IQueryable Szulok type.</returns>
        public IQueryable<Szulok> Listazas()
        {
            return this.repo.Listazas();
        }

        /// <summary>
        /// INSERT ujSzulo.
        /// </summary>
        /// <param name="ujSzulo">Új Szülő.</param>
        public void Hozzaadas(Szulok ujSzulo)
        {
            this.repo.Hozzaadas(ujSzulo);
        }

        /// <summary>
        /// UPDATE EMAIL TO uj_email.
        /// </summary>
        /// <param name="id">SzuloID.</param>
        /// <param name="uj_email">Új email.</param>
        public void Email_Modositas(string id, string uj_email)
        {
            this.repo.Email_Modositas(id, uj_email);
        }

        /// <summary>
        /// UPDATE HIRLEVEL TO uj_hirlevel.
        /// </summary>
        /// <param name="id">SzuloID.</param>
        /// <param name="uj_hirlevel">Új hírlevél státusz.</param>
        public void Hirlevel_Modositas(string id, short uj_hirlevel)
        {
            this.repo.Hirlevel_Modositas(id, uj_hirlevel);
        }

        /// <summary>
        /// UPDATE ID TO id.
        /// </summary>
        /// <param name="id">SzuloID.</param>
        /// <param name="uj_id">Új Szülő ID.</param>
        public void ID_Modositas(string id, string uj_id)
        {
            this.repo.ID_Modositas(id, uj_id);
        }

        /// <summary>
        /// UPDATE NEV TO uj_nev.
        /// </summary>
        /// <param name="id">SzuloID.</param>
        /// <param name="uj_nev">Új név.</param>
        public void Nev_Modositas(string id, string uj_nev)
        {
            this.repo.Nev_Modositas(id, uj_nev);
        }

        /// <summary>
        /// UPDATE TELEFONSZAM TO uj_telefon.
        /// </summary>
        /// <param name="id">SzuloID.</param>
        /// <param name="uj_telefon">Új telefonszám.</param>
        public void Telefonszam_Modositas(string id, string uj_telefon)
        {
            this.repo.Telefonszam_Modositas(id, uj_telefon);
        }

        /// <summary>
        /// UPDATE TELEPULES TO uj_telepules.
        /// </summary>
        /// <param name="id">SzuloID.</param>
        /// <param name="uj_telepules">Új település.</param>
        public void Telepules_Modositas(string id, string uj_telepules)
        {
            this.repo.Telepules_Modositas(id, uj_telepules);
        }

        /// <summary>
        /// DELETE BY ID.
        /// </summary>
        /// <param name="id">SzuloID.</param>
        public void Torles(string id)
        {
            this.repo.Torles(id);
        }

        /// <summary>
        /// SELECT BY ID.
        /// </summary>
        /// <param name="id">SzuloID.</param>
        /// <returns>Az adott ID-jú Szülő.</returns>
        public Szulok GetOne(string id)
        {
            return this.repo.GetOne(id);
        }
    }
}
