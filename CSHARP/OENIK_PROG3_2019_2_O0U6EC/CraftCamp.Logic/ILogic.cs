﻿// <copyright file="ILogic.cs" company="Nagy Dávid O0U6EC">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CraftCamp.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CraftCamp.Data;
    using CraftCamp.Logic.NonCrudClasses;

    /// <summary>
    /// ILogic interface.
    /// </summary>
    /// <typeparam name="T">Entity type.</typeparam>
    public interface ILogic<T>
        where T : class
    {
        /// <summary>
        /// SELECT ALL.
        /// </summary>
        /// <returns>IQueryable T type.</returns>
        IQueryable<T> Listazas();
    }

    /// <summary>
    /// ISzuloLogic interface.
    /// </summary>
    public interface ISzuloLogic : ILogic<Szulok>
    {
        /// <summary>
        /// SELECT BY ID.
        /// </summary>
        /// <param name="id">SZ_ID.</param>
        /// <returns>Szulok type.</returns>
        Szulok GetOne(string id);

        /// <summary>
        /// INSERT.
        /// </summary>
        /// <param name="ujSzulo">Új Szülő.</param>
        void Hozzaadas(Szulok ujSzulo);

        /// <summary>
        /// UPDATE EMAIL BY ID.
        /// </summary>
        /// <param name="id">SZ_ID.</param>
        /// <param name="uj_email">Új Email.</param>
        void Email_Modositas(string id, string uj_email);

        /// <summary>
        /// UPDATE HIRLEVEL BY ID.
        /// </summary>
        /// <param name="id">SZ_ID.</param>
        /// <param name="uj_hirlevel">Új hírlevél státusz.</param>
        void Hirlevel_Modositas(string id, short uj_hirlevel);

        /// <summary>
        /// UPDATE ID BY ID.
        /// </summary>
        /// <param name="id">SZ_ID.</param>
        /// <param name="uj_id">Új SZ_ID.</param>
        void ID_Modositas(string id, string uj_id);

        /// <summary>
        /// UPDATE NEV BY ID.
        /// </summary>
        /// <param name="id">SZ_ID.</param>
        /// <param name="uj_nev">Új név.</param>
        void Nev_Modositas(string id, string uj_nev);

        /// <summary>
        /// UPDATE TELEFONSZAM BY ID.
        /// </summary>
        /// <param name="id">SZ_ID.</param>
        /// <param name="uj_telefon">Új telefonszám.</param>
        void Telefonszam_Modositas(string id, string uj_telefon);

        /// <summary>
        /// UPDATE TELEPULES BY ID.
        /// </summary>
        /// <param name="id">SZ_ID.</param>
        /// <param name="uj_telepules">Új település.</param>
        void Telepules_Modositas(string id, string uj_telepules);

        /// <summary>
        /// DELETE SZULO BY ID.
        /// </summary>
        /// <param name="id">SZ_ID.</param>
        void Torles(string id);
    }

    /// <summary>
    /// IGyerekLogic interface.
    /// </summary>
    public interface IGyerekLogic : ILogic<Gyerekek>
    {
        /// <summary>
        /// SELECT BY ID.
        /// </summary>
        /// <param name="id">Gyerek_ID.</param>
        /// <returns>A kiválasztott Gyerek.</returns>
        Gyerekek GetOne(string id);

        /// <summary>
        /// INSERT GYEREK.
        /// </summary>
        /// <param name="ujGyerek">Új Gyerek.</param>
        void Hozzaadas(Gyerekek ujGyerek);

        /// <summary>
        /// UPDATE ID BY ID. (MC_USER).
        /// </summary>
        /// <param name="id">Gyerek_ID.</param>
        /// <param name="uj_id">Gyerek új ID.</param>
        void MC_Modositas(string id, string uj_id);

        /// <summary>
        /// UPDATE NEV BY ID.
        /// </summary>
        /// <param name="id">Gyerek_ID.</param>
        /// <param name="uj_nev">Új név.</param>
        void Nev_Modositas(string id, string uj_nev);

        /// <summary>
        /// UPDATE KOR BY ID.
        /// </summary>
        /// <param name="id">Gyerek_ID.</param>
        /// <param name="uj_kor">Új kor.</param>
        void Kor_Modositas(string id, string uj_kor);

        /// <summary>
        /// UPDATE SZ_ID (FK) BY ID.
        /// </summary>
        /// <param name="id">Gyerek_ID.</param>
        /// <param name="uj_SZ">Új SZ_ID.</param>
        void SZ_Modositas(string id, string uj_SZ);

        /// <summary>
        /// DELETE BY ID.
        /// </summary>
        /// <param name="id">Gyerek_ID.</param>
        void Torles(string id);
    }

    /// <summary>
    /// ITaborLogic Interface.
    /// </summary>
    public interface ITaborLogic : ILogic<Taborok>
    {
        /// <summary>
        /// SELECT BY ID.
        /// </summary>
        /// <param name="id">TáborID.</param>
        /// <returns>A kiválasztott Tábor.</returns>
        Taborok GetOne(long id);

        /// <summary>
        /// UPDATE BEFEJEZES BY ID.
        /// </summary>
        /// <param name="id">TáborID.</param>
        /// <param name="uj_befejezes">Új befejezés.</param>
        void Befejezes_Modositas(long id, DateTime uj_befejezes);

        /// <summary>
        /// UPDATE HELYSZÍN_ID BY ID.
        /// </summary>
        /// <param name="id">TáborID.</param>
        /// <param name="uj_hID">Új HelyszínID.</param>
        void HID_Modositas(long id, long uj_hID);

        /// <summary>
        /// INSERT TÁBOR.
        /// </summary>
        /// <param name="ujTabor">Új Tábor.</param>
        void Hozzaadas(Taborok ujTabor);

        /// <summary>
        /// UPDATE ID BY ID.
        /// </summary>
        /// <param name="id">TáborID.</param>
        /// <param name="uj_id">Új TáborID.</param>
        void ID_Modositas(long id, long uj_id);

        /// <summary>
        /// UPDATE KEZDES BY ID.
        /// </summary>
        /// <param name="id">TáborID.</param>
        /// <param name="uj_kezdes">Új kezdés.</param>
        void Kezdes_Modositas(long id, DateTime uj_kezdes);

        /// <summary>
        /// UPDATE MAXFO BY ID.
        /// </summary>
        /// <param name="id">TáborID.</param>
        /// <param name="uj_maxfo">Új Maxfő.</param>
        void Maxfo_Modositas(long id, long uj_maxfo);

        /// <summary>
        /// UPDATE NEV BY ID.
        /// </summary>
        /// <param name="id">TáborID.</param>
        /// <param name="uj_nev">Új név.</param>
        void Nev_Modositas(long id, string uj_nev);

        /// <summary>
        /// UPDATE SZABADHELYEK BY ID.
        /// </summary>
        /// <param name="id">TáborID.</param>
        /// <param name="uj_szabadHelyek">Új szabadhelyek.</param>
        void SzabadHelyek_Modositas(long id, long uj_szabadHelyek);

        /// <summary>
        /// DELETE BY ID.
        /// </summary>
        /// <param name="id">TáborID.</param>
        void Torles(long id);
    }

    /// <summary>
    /// IHelyszinLogic Interface.
    /// </summary>
    public interface IHelyszinLogic : ILogic<Helyszinek>
    {
        /// <summary>
        /// SELECT BY ID.
        /// </summary>
        /// <param name="id">HelyszínID.</param>
        /// <returns>A kiválasztott Helyszín.</returns>
        Helyszinek GetOne(long id);

        /// <summary>
        /// INSERT Helyszín.
        /// </summary>
        /// <param name="ujHelyszin">Új Helyszín.</param>
        void Hozzaadas(Helyszinek ujHelyszin);

        /// <summary>
        /// UPDATE ID BY ID.
        /// </summary>
        /// <param name="id">HelyszínID.</param>
        /// <param name="uj_id">Új HelyszínID.</param>
        void ID_Modositas(long id, long uj_id);

        /// <summary>
        /// UPDATE MAXHELY BY ID.
        /// </summary>
        /// <param name="id">HelyszínID.</param>
        /// <param name="uj_maxhely">Új Maxhely.</param>
        void Maxhely_Modositas(long id, long uj_maxhely);

        /// <summary>
        /// UPDATE NEV BY ID.
        /// </summary>
        /// <param name="id">HelyszínID.</param>
        /// <param name="uj_nev">Új név.</param>
        void Nev_Modositas(long id, string uj_nev);

        /// <summary>
        /// UPDATE TELEFONSZAM BY ID.
        /// </summary>
        /// <param name="id">HelyszínID.</param>
        /// <param name="uj_telefon">Új Telefonszám.</param>
        void Telefonszam_Modositas(long id, string uj_telefon);

        /// <summary>
        /// UPDATE TELEPULES BY ID.
        /// </summary>
        /// <param name="id">HelyszínID.</param>
        /// <param name="uj_telepules">Új település.</param>
        void Telepules_Modositas(long id, string uj_telepules);

        /// <summary>
        /// UPDATE UTCA BY ID.
        /// </summary>
        /// <param name="id">HelyszínID.</param>
        /// <param name="uj_utca">Új utca.</param>
        void Utca_Modositas(long id, string uj_utca);

        /// <summary>
        /// DELETE BY ID.
        /// </summary>
        /// <param name="id">HelyszínID.</param>
        /// <returns>TRUE/FALSE based on success or failure.</returns>
        bool Torles(long id);
    }

    /// <summary>
    /// Interface a nonCRUDLOGIC-hoz.
    /// </summary>
    public interface InonCRUDLOGIC
    {
        /// <summary>
        /// Kiválasztja azokat a szülőket, akik fel vannak iratkozva hírlevélre és egy Listában visszaadja Őket.
        /// </summary>
        /// <returns>Lista, amely 'HirlevelesSzulok'-et tartalmaz.</returns>
        List<HirlevelesSzulo> HirlevelesSzulo();

        /// <summary>
        /// Kiírja, hogy az egyes Tábortípusok átlagosan milyen maxlétszámmal indulnak.
        /// </summary>
        /// <returns>Lista, amely a Tábortípusokat és a hozzá tartozó Átlagos maxfőket tartalmazza.</returns>
        List<TaborAtlagMaxfo> AtlagMaxfoTaborTipusonkent();

        /// <summary>
        /// Kiírja, hogy az egyes Táborok mely helyszíneken lettek megrendezve.
        /// </summary>
        /// <returns>Lista, amely a tábort és a neki megfelelő helyszínt tartalmazza.</returns>
        List<TaborokHelyszinek> TaborokEsHelyszinek();
    }
}
