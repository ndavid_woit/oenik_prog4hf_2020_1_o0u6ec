﻿// <copyright file="LogicTests.cs" company="Nagy Dávid O0U6EC">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CraftCamp.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CraftCamp.Data;
    using CraftCamp.Logic.NonCrudClasses;
    using CraftCamp.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Logic teszteléséhez létrehozott osztály.
    /// </summary>
    [TestFixture]
    internal class LogicTests
    {
        /// <summary>
        /// SELECT ALL Taborok tesztelése.
        /// </summary>
        [Test]
        public void TestGetAllTaborok()
        {
            // ARRANGE
            Mock<ITaborRepository> mockedRepository = new Mock<ITaborRepository>();

            List<Taborok> taborok = new List<Taborok>
            {
                new Taborok() { ID = 1, Nev = "History", Kezdes = DateTime.Parse("2019.07.01"), Befejezes = DateTime.Parse("2019.07.08"), Szabad_helyek = 16, Maxfo = 16, H_ID = 2 },
                new Taborok() { ID = 2, Nev = "Halloween", Kezdes = DateTime.Parse("2019.10.28"), Befejezes = DateTime.Parse("2019.10.31"), Szabad_helyek = 13, Maxfo = 13, H_ID = 5 },
                new Taborok() { ID = 3, Nev = "SummerTime", Kezdes = DateTime.Parse("2019.08.10"), Befejezes = DateTime.Parse("2019.08.16"), Szabad_helyek = 13, Maxfo = 13, H_ID = 1 },
                new Taborok() { ID = 4, Nev = "History", Kezdes = DateTime.Parse("2019.07.12"), Befejezes = DateTime.Parse("2019.07.19"), Szabad_helyek = 15, Maxfo = 15, H_ID = 3 },
                new Taborok() { ID = 5, Nev = "Öko", Kezdes = DateTime.Parse("2019.07.20"), Befejezes = DateTime.Parse("2019.07.25"), Szabad_helyek = 10, Maxfo = 10, H_ID = 3 },
            };
            mockedRepository.Setup(r => r.Listazas()).Returns(taborok.AsQueryable());
            List<string> lista = new List<string> { "History", "Halloween", "SummerTime", "History", "Öko" };
            TaborLogic tLogic = new TaborLogic(mockedRepository.Object);

            // ACT
            List<Taborok> result = tLogic.Listazas().ToList();

            // ASSERT
            mockedRepository.Verify(r => r.Listazas(), Times.Once);
            Assert.That(result.Select(x => x.Nev), Is.EqualTo(lista));
            Assert.That(result.Count(), Is.EqualTo(5));
            Assert.That(result.Select(x => x.ID), Is.Unique);
        }

        /// <summary>
        /// Új helyszín hozzáadásának tesztelése.
        /// </summary>
        [Test]
        public void TestHelyszinHozzaadas()
        {
            // ARRANGE
            Mock<IHelyszinRepository> mockedRepo = new Mock<IHelyszinRepository>();

            List<Helyszinek> helyszinek = new List<Helyszinek>
            {
                new Helyszinek() { ID = 1, Nev = "FunnyHouse Közösségi Ház" },
                new Helyszinek() { ID = 2, Nev = "Entranet Alapítvány" },
                new Helyszinek() { ID = 3, Nev = "ÚjNemzedék közösségi tér" },
                new Helyszinek() { ID = 4, Nev = "Városi Művelődési központ" },
                new Helyszinek() { ID = 5, Nev = "Megyei Könyvtár" },
            };

            Helyszinek ujHelyszin = new Helyszinek() { ID = 6, Nev = "EzTesztHouse" };

            mockedRepo.Setup(r => r.Hozzaadas(It.IsAny<Helyszinek>())).Callback<Helyszinek>((h) =>
            {
                helyszinek.Add(h);
            });
            HelyszinLogic hLogic = new HelyszinLogic(mockedRepo.Object);

            // ACT
            hLogic.Hozzaadas(ujHelyszin);

            // ASSERT
            mockedRepo.Verify(repo => repo.Hozzaadas(It.IsAny<Helyszinek>()), Times.Exactly(1));
            Assert.DoesNotThrow(() => hLogic.Hozzaadas(It.IsAny<Helyszinek>()));
            Assert.That(helyszinek.Select(x => x.ID), Contains.Item(ujHelyszin.ID));
        }

        /// <summary>
        /// GyerekNév módosításának tesztelése.
        /// </summary>
        /// <param name="id">GyerekID.</param>
        /// <param name="nev">Gyerek Új neve.</param>
        [TestCase("Kicsike26", "Teszt Zsolt")]
        public void TestGyerekNevModositas(string id, string nev)
        {
            // ARRANGE
            Mock<IGyerekRepository> mockedRepo = new Mock<IGyerekRepository>();

            List<Gyerekek> gyerekek = new List<Gyerekek>
            {
                new Gyerekek() { MC_USER = "Kicsike26", Nev = "TesztElek" },
                new Gyerekek() { MC_USER = "Emanuel47", Nev = "Nagy Manuel" },
            };

            List<string> expected = new List<string> { "TesztElek", "Nagy Manuel" };

            mockedRepo.Setup(r => r.Nev_Modositas(id, It.IsAny<string>())).Callback<string, string>((a, b) =>
            {
                gyerekek.SingleOrDefault(s => s.MC_USER == a).Nev = b;
            });

            GyerekLogic gyLogic = new GyerekLogic(mockedRepo.Object);

            // ACT
            gyLogic.Nev_Modositas(id, nev);

            // ASSERT
            Assert.That(gyerekek.Select(x => x.MC_USER), Contains.Item(id));
            mockedRepo.Verify(repo => repo.Nev_Modositas(It.IsAny<string>(), It.IsAny<string>()), Times.Exactly(1));
            Assert.DoesNotThrow(() => gyLogic.Nev_Modositas(It.IsAny<string>(), It.IsAny<string>()));
            Assert.That(gyerekek.Select(x => x.Nev), Contains.Item("Teszt Zsolt"));
            Assert.That(gyerekek.Select(x => x.Nev), Does.Not.Contain("TesztElek"));
        }

        /// <summary>
        /// Szülő törlésének tesztelése.
        /// </summary>
        /// <param name="id">SzülőID.</param>
        [TestCase("456123ER")]
        public void TestSzuloTorles(string id)
        {
            // ARRANGE
            Mock<ISzuloRepository> mockedRepo = new Mock<ISzuloRepository>();

            List<Szulok> szulok = new List<Szulok>
            {
                new Szulok() { SZ_ID = "456123ER", Nev = "Teszt SZULO" },
                new Szulok() { SZ_ID = "222333DA", Nev = "Nagy Manuel" },
            };

            mockedRepo.Setup(r => r.Torles(It.IsAny<string>())).Callback<string>((c) => szulok.RemoveAll(x => x.SZ_ID == c));

            SzuloLogic szLogic = new SzuloLogic(mockedRepo.Object);

            // ACT
            szLogic.Torles(id);

            // ASSERT
            mockedRepo.Verify(repo => repo.Torles(It.IsAny<string>()));
            Assert.That(szulok.Select(x => x.SZ_ID), Does.Not.Contain(id));
            Assert.That(szulok.Count(), Is.EqualTo(1));
        }

        /// <summary>
        /// SELECT BY ID tesztelése (Szülőn).
        /// </summary>
        /// <param name="id">SzülőID.</param>
        [TestCase("123")]
        public void TestSzuloGetOne(string id)
        {
            // ARRANGE
            Mock<ISzuloRepository> mockedRepo = new Mock<ISzuloRepository>();

            List<Szulok> szulok = new List<Szulok>
            {
                new Szulok() { SZ_ID = "123", Nev = "Teszt SZULO", Telefonszam = "06204114523" },
                new Szulok() { SZ_ID = "222333DA", Nev = "Nagy Manuel", Telefonszam = "+36201125587" },
            };

            mockedRepo.Setup(r => r.GetOne(It.IsAny<string>())).Returns<string>(szid => szulok.SingleOrDefault(r => r.SZ_ID == szid));
            SzuloLogic szLogic = new SzuloLogic(mockedRepo.Object);

            // ACT
            var selectedSzulo = szLogic.GetOne(id);

            // ASSERT
            mockedRepo.Verify(repo => repo.GetOne(id), Times.Once);
            Assert.DoesNotThrow(() => szLogic.GetOne(id));
            Assert.IsNotNull(selectedSzulo);
            Assert.That(id, Is.EqualTo(selectedSzulo.SZ_ID));
            Assert.That(szulok, Contains.Item(selectedSzulo));
        }

        /// <summary>
        /// HírlevelesSzülő() nonCRUDLogic tesztelése.
        /// </summary>
        [Test]
        public void TestHirlevelesSzulok()
        {
            Mock<ISzuloRepository> mockedRepo = new Mock<ISzuloRepository>();

            List<Szulok> szulok = new List<Szulok>
            {
                new Szulok() { SZ_ID = "456123ER", Nev = "Teszt SZULO", Telefonszam = "06204114523", Hirlevel = 1 },
                new Szulok() { SZ_ID = "222333DA", Nev = "Nagy Manuel", Telefonszam = "+36201125587", Hirlevel = 0 },
                new Szulok() { SZ_ID = "456123ER", Nev = "Teszt Elek", Telefonszam = "06204114523", Hirlevel = 1 },
            };

            mockedRepo.Setup(r => r.Listazas()).Returns(szulok.AsQueryable());

            NonCRUDLogic logic = new NonCRUDLogic(mockedRepo.Object);

            var hirlevelesek = logic.HirlevelesSzulo();

            mockedRepo.Verify(repo => repo.Listazas(), Times.Once);
            Assert.That(hirlevelesek.Select(y => y.Hirlevel), Does.Not.Contains(0));
        }

        /// <summary>
        /// AtlagMaxfoTaborTipusonkent() nonCRUDLogic tesztelése.
        /// </summary>
        [Test]
        public void TestAtlagMaxfoTaborTipusonkent()
        {
            Mock<ITaborRepository> mockedRepo = new Mock<ITaborRepository>();

            List<Taborok> taborok = new List<Taborok>
            {
                new Taborok() { ID = 1, Nev = "History", Kezdes = DateTime.Parse("2019.07.01"), Befejezes = DateTime.Parse("2019.07.08"), Szabad_helyek = 16, Maxfo = 16, H_ID = 2 },
                new Taborok() { ID = 2, Nev = "Halloween", Kezdes = DateTime.Parse("2019.10.28"), Befejezes = DateTime.Parse("2019.10.31"), Szabad_helyek = 13, Maxfo = 13, H_ID = 5 },
                new Taborok() { ID = 3, Nev = "SummerTime", Kezdes = DateTime.Parse("2019.08.10"), Befejezes = DateTime.Parse("2019.08.16"), Szabad_helyek = 13, Maxfo = 13, H_ID = 1 },
                new Taborok() { ID = 4, Nev = "History", Kezdes = DateTime.Parse("2019.07.12"), Befejezes = DateTime.Parse("2019.07.19"), Szabad_helyek = 15, Maxfo = 15, H_ID = 3 },
                new Taborok() { ID = 5, Nev = "History", Kezdes = DateTime.Parse("2019.07.20"), Befejezes = DateTime.Parse("2019.07.25"), Szabad_helyek = 10, Maxfo = 11, H_ID = 3 },
            };

            List<TaborAtlagMaxfo> expected = new List<TaborAtlagMaxfo>
            {
                new TaborAtlagMaxfo() { Tabor = "History", AtlagMaxfo = 14 },
                new TaborAtlagMaxfo() { Tabor = "Halloween", AtlagMaxfo = 13 },
                new TaborAtlagMaxfo() { Tabor = "SummerTime", AtlagMaxfo = 13 },
            };

            mockedRepo.Setup(r => r.Listazas()).Returns(taborok.AsQueryable());

            NonCRUDLogic logic = new NonCRUDLogic(mockedRepo.Object);

            var result = logic.AtlagMaxfoTaborTipusonkent();

            mockedRepo.Verify(repo => repo.Listazas(), Times.Once);
            Assert.That(result.Count(), Is.EqualTo(3));
            Assert.That(result, Is.EquivalentTo(expected));
        }

        /// <summary>
        /// TaborokEsHelyszinek() nonCRUDLogic tesztelése.
        /// </summary>
        [Test]
        public void TestTaborokEsHelyszinek()
        {
            Mock<ITaborRepository> taborMockedRepo = new Mock<ITaborRepository>();
            Mock<IHelyszinRepository> helyszinMockedRepo = new Mock<IHelyszinRepository>();

            List<Taborok> taborok = new List<Taborok>
            {
                new Taborok() { ID = 1, Nev = "History", Kezdes = DateTime.Parse("2019.07.01"), Befejezes = DateTime.Parse("2019.07.08"), Szabad_helyek = 16, Maxfo = 16, H_ID = 2 },
                new Taborok() { ID = 2, Nev = "Halloween", Kezdes = DateTime.Parse("2019.10.28"), Befejezes = DateTime.Parse("2019.10.31"), Szabad_helyek = 13, Maxfo = 13, H_ID = 5 },
                new Taborok() { ID = 3, Nev = "SummerTime", Kezdes = DateTime.Parse("2019.08.10"), Befejezes = DateTime.Parse("2019.08.16"), Szabad_helyek = 13, Maxfo = 13, H_ID = 1 },
                new Taborok() { ID = 4, Nev = "History", Kezdes = DateTime.Parse("2019.07.12"), Befejezes = DateTime.Parse("2019.07.19"), Szabad_helyek = 15, Maxfo = 15, H_ID = 3 },
                new Taborok() { ID = 5, Nev = "History", Kezdes = DateTime.Parse("2019.07.20"), Befejezes = DateTime.Parse("2019.07.25"), Szabad_helyek = 10, Maxfo = 11, H_ID = 3 },
            };

            List<Helyszinek> helyszinek = new List<Helyszinek>
            {
                new Helyszinek() { ID = 1, Nev = "FunnyHouse Közösségi Ház" },
                new Helyszinek() { ID = 2, Nev = "Entranet Alapítvány" },
                new Helyszinek() { ID = 3, Nev = "ÚjNemzedék közösségi tér" },
                new Helyszinek() { ID = 4, Nev = "Városi Művelődési központ" },
                new Helyszinek() { ID = 5, Nev = "Megyei Könyvtár" },
            };
            TaborokHelyszinek test = new TaborokHelyszinek() { Tabor = "History", Helyszin = "Entranet Alapítvány" };

            taborMockedRepo.Setup(repo => repo.Listazas()).Returns(taborok.AsQueryable);
            helyszinMockedRepo.Setup(repo => repo.Listazas()).Returns(helyszinek.AsQueryable);

            NonCRUDLogic logic = new NonCRUDLogic(helyszinMockedRepo.Object, taborMockedRepo.Object);

            var actual = logic.TaborokEsHelyszinek();

            Assert.That(actual.Count(), Is.EqualTo(5));
            Assert.That(actual, Contains.Item(test));
            taborMockedRepo.Verify(repo => repo.Listazas(), Times.Once);
            helyszinMockedRepo.Verify(repo => repo.Listazas(), Times.Once);
        }
    }
}
