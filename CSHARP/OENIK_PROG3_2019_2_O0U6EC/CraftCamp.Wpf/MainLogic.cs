﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CraftCamp.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:60631/api/PlacesApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "PlaceResult");
        }

        public List<PlacesVM> ApiGetPlaces()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<PlacesVM>>(json);
            return list;
        }

        public void ApiDelPlace(PlacesVM place)
        {
            bool success = false;
            if (place != null)
            {
                string json = client.GetStringAsync(url + "delete/" + place.ID.ToString()).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditPlace(PlacesVM place, bool isEditing)
        {
            if (place == null) 
                return false;
            string Url = isEditing ? url + "edit" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing) 
                postData.Add(nameof(PlacesVM.ID), place.ID.ToString());

            postData.Add(nameof(PlacesVM.Name), place.Name);
            postData.Add(nameof(PlacesVM.Phone), place.Phone);
            postData.Add(nameof(PlacesVM.Capacity), place.Capacity.ToString());
            postData.Add(nameof(PlacesVM.City), place.City);
            postData.Add(nameof(PlacesVM.Street), place.Street);


            string json = client.PostAsync(Url, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

        public void EditPlace(PlacesVM place, Func<PlacesVM, bool> editor)
        {
            PlacesVM clone = new PlacesVM();
            if (place != null) 
                clone.CopyFrom(place);
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (place != null) 
                    success = ApiEditPlace(clone, true); // Edit place!

                else success = ApiEditPlace(clone, false); // Add new Place!
            }
            SendMessage(success == true);
        }
    }
}
