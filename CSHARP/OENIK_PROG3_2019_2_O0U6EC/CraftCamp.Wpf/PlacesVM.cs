﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CraftCamp.Wpf
{
    class PlacesVM : ObservableObject
    {
		private int id;
		private string name;
		private string phone;
		private int capacity;
		private string city;
		private string street;

		public string Street
		{
			get { return street; }
			set { Set(ref street, value); }
		}


		public string City
		{
			get { return city; }
			set { Set(ref city, value); }
		}


		public int Capacity
		{
			get { return capacity; }
			set { Set(ref capacity, value); }
		}


		public string Phone
		{
			get { return phone; }
			set { Set(ref phone, value); }
		}


		public string Name
		{
			get { return name; }
			set { Set(ref name, value); }
		}


		public int ID
		{
			get { return id; }
			set { Set(ref id, value); }
		}

		public void CopyFrom(PlacesVM other)
		{
			if (other == null) 
				return;
			this.ID = other.ID;
			this.Name = other.Name;
			this.Phone = other.Phone;
			this.Capacity = other.Capacity;
			this.City = other.City;
			this.Street = other.Street;
		}

	}
}
