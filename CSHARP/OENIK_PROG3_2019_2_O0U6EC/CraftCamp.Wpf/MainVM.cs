﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CraftCamp.Wpf
{
    class MainVM : ViewModelBase
    {
		private MainLogic logic;
		private ObservableCollection<PlacesVM> allPlaces;
		private PlacesVM selectedPlace;

		public PlacesVM SelectedPlace
		{
			get { return selectedPlace; }
			set { Set(ref selectedPlace, value); }
		}
		public ObservableCollection<PlacesVM> AllPlaces
		{
			get { return allPlaces; }
			set { Set(ref allPlaces, value); }
		}

		public ICommand AddCmd { get; private set; }
		public ICommand DeleteCmd { get; private set; }
		public ICommand EditCmd { get; private set; }
		public ICommand LoadCmd { get; private set; }

		public Func<PlacesVM, bool> EditorFunc { get; set; }

		public MainVM()
		{
			logic = new MainLogic();

			LoadCmd = new RelayCommand(() =>
					AllPlaces = new ObservableCollection<PlacesVM>(logic.ApiGetPlaces()));
			DeleteCmd = new RelayCommand(() => logic.ApiDelPlace(selectedPlace));
			AddCmd = new RelayCommand(() => logic.EditPlace(null, EditorFunc));
			EditCmd = new RelayCommand(() => logic.EditPlace(selectedPlace, EditorFunc));
		}
	}
}
