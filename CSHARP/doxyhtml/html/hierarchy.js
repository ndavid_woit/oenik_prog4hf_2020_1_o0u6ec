var hierarchy =
[
    [ "DbContext", null, [
      [ "CraftCamp::Data::CraftCampDBEntities", "class_craft_camp_1_1_data_1_1_craft_camp_d_b_entities.html", null ]
    ] ],
    [ "CraftCamp.Data.Gyerekek", "class_craft_camp_1_1_data_1_1_gyerekek.html", null ],
    [ "CraftCamp.Data.Helyszinek", "class_craft_camp_1_1_data_1_1_helyszinek.html", null ],
    [ "CraftCamp.Logic.NonCrudClasses.HirlevelesSzulo", "class_craft_camp_1_1_logic_1_1_non_crud_classes_1_1_hirleveles_szulo.html", null ],
    [ "CraftCamp.Logic.ILogic< T >", "interface_craft_camp_1_1_logic_1_1_i_logic.html", null ],
    [ "CraftCamp.Logic.ILogic< Gyerekek >", "interface_craft_camp_1_1_logic_1_1_i_logic.html", [
      [ "CraftCamp.Logic.IGyerekLogic", "interface_craft_camp_1_1_logic_1_1_i_gyerek_logic.html", [
        [ "CraftCamp.Logic.GyerekLogic", "class_craft_camp_1_1_logic_1_1_gyerek_logic.html", null ]
      ] ]
    ] ],
    [ "CraftCamp.Logic.ILogic< Helyszinek >", "interface_craft_camp_1_1_logic_1_1_i_logic.html", [
      [ "CraftCamp.Logic.IHelyszinLogic", "interface_craft_camp_1_1_logic_1_1_i_helyszin_logic.html", [
        [ "CraftCamp.Logic.HelyszinLogic", "class_craft_camp_1_1_logic_1_1_helyszin_logic.html", null ]
      ] ]
    ] ],
    [ "CraftCamp.Logic.ILogic< Szulok >", "interface_craft_camp_1_1_logic_1_1_i_logic.html", [
      [ "CraftCamp.Logic.ISzuloLogic", "interface_craft_camp_1_1_logic_1_1_i_szulo_logic.html", [
        [ "CraftCamp.Logic.SzuloLogic", "class_craft_camp_1_1_logic_1_1_szulo_logic.html", null ]
      ] ]
    ] ],
    [ "CraftCamp.Logic.ILogic< Taborok >", "interface_craft_camp_1_1_logic_1_1_i_logic.html", [
      [ "CraftCamp.Logic.ITaborLogic", "interface_craft_camp_1_1_logic_1_1_i_tabor_logic.html", [
        [ "CraftCamp.Logic.TaborLogic", "class_craft_camp_1_1_logic_1_1_tabor_logic.html", null ]
      ] ]
    ] ],
    [ "CraftCamp.Logic.InonCRUDLOGIC", "interface_craft_camp_1_1_logic_1_1_inon_c_r_u_d_l_o_g_i_c.html", [
      [ "CraftCamp.Logic.NonCRUDLogic", "class_craft_camp_1_1_logic_1_1_non_c_r_u_d_logic.html", null ]
    ] ],
    [ "CraftCamp.Repository.IRepository< T >", "interface_craft_camp_1_1_repository_1_1_i_repository.html", [
      [ "CraftCamp.Repository.Repository< T >", "class_craft_camp_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "CraftCamp.Repository.IRepository< Gyerekek >", "interface_craft_camp_1_1_repository_1_1_i_repository.html", [
      [ "CraftCamp.Repository.IGyerekRepository", "interface_craft_camp_1_1_repository_1_1_i_gyerek_repository.html", [
        [ "CraftCamp.Repository.GyerekRepository", "class_craft_camp_1_1_repository_1_1_gyerek_repository.html", null ]
      ] ]
    ] ],
    [ "CraftCamp.Repository.IRepository< Helyszinek >", "interface_craft_camp_1_1_repository_1_1_i_repository.html", [
      [ "CraftCamp.Repository.IHelyszinRepository", "interface_craft_camp_1_1_repository_1_1_i_helyszin_repository.html", [
        [ "CraftCamp.Repository.HelyszinRepository", "class_craft_camp_1_1_repository_1_1_helyszin_repository.html", null ]
      ] ]
    ] ],
    [ "CraftCamp.Repository.IRepository< Szulok >", "interface_craft_camp_1_1_repository_1_1_i_repository.html", [
      [ "CraftCamp.Repository.ISzuloRepository", "interface_craft_camp_1_1_repository_1_1_i_szulo_repository.html", [
        [ "CraftCamp.Repository.SzuloRepository", "class_craft_camp_1_1_repository_1_1_szulo_repository.html", null ]
      ] ]
    ] ],
    [ "CraftCamp.Repository.IRepository< Taborok >", "interface_craft_camp_1_1_repository_1_1_i_repository.html", [
      [ "CraftCamp.Repository.ITaborRepository", "interface_craft_camp_1_1_repository_1_1_i_tabor_repository.html", [
        [ "CraftCamp.Repository.TaborRepository", "class_craft_camp_1_1_repository_1_1_tabor_repository.html", null ]
      ] ]
    ] ],
    [ "CraftCamp.Data.Jelentkezesek", "class_craft_camp_1_1_data_1_1_jelentkezesek.html", null ],
    [ "CraftCamp.Repository.Repository< Gyerekek >", "class_craft_camp_1_1_repository_1_1_repository.html", [
      [ "CraftCamp.Repository.GyerekRepository", "class_craft_camp_1_1_repository_1_1_gyerek_repository.html", null ]
    ] ],
    [ "CraftCamp.Repository.Repository< Helyszinek >", "class_craft_camp_1_1_repository_1_1_repository.html", [
      [ "CraftCamp.Repository.HelyszinRepository", "class_craft_camp_1_1_repository_1_1_helyszin_repository.html", null ]
    ] ],
    [ "CraftCamp.Repository.Repository< Szulok >", "class_craft_camp_1_1_repository_1_1_repository.html", [
      [ "CraftCamp.Repository.SzuloRepository", "class_craft_camp_1_1_repository_1_1_szulo_repository.html", null ]
    ] ],
    [ "CraftCamp.Repository.Repository< Taborok >", "class_craft_camp_1_1_repository_1_1_repository.html", [
      [ "CraftCamp.Repository.TaborRepository", "class_craft_camp_1_1_repository_1_1_tabor_repository.html", null ]
    ] ],
    [ "CraftCamp.Data.Szulok", "class_craft_camp_1_1_data_1_1_szulok.html", null ],
    [ "CraftCamp.Logic.NonCrudClasses.TaborAtlagMaxfo", "class_craft_camp_1_1_logic_1_1_non_crud_classes_1_1_tabor_atlag_maxfo.html", null ],
    [ "CraftCamp.Data.Taborok", "class_craft_camp_1_1_data_1_1_taborok.html", null ],
    [ "CraftCamp.Logic.NonCrudClasses.TaborokHelyszinek", "class_craft_camp_1_1_logic_1_1_non_crud_classes_1_1_taborok_helyszinek.html", null ]
];