var namespace_craft_camp_1_1_logic =
[
    [ "NonCrudClasses", "namespace_craft_camp_1_1_logic_1_1_non_crud_classes.html", "namespace_craft_camp_1_1_logic_1_1_non_crud_classes" ],
    [ "GyerekLogic", "class_craft_camp_1_1_logic_1_1_gyerek_logic.html", "class_craft_camp_1_1_logic_1_1_gyerek_logic" ],
    [ "HelyszinLogic", "class_craft_camp_1_1_logic_1_1_helyszin_logic.html", "class_craft_camp_1_1_logic_1_1_helyszin_logic" ],
    [ "IGyerekLogic", "interface_craft_camp_1_1_logic_1_1_i_gyerek_logic.html", "interface_craft_camp_1_1_logic_1_1_i_gyerek_logic" ],
    [ "IHelyszinLogic", "interface_craft_camp_1_1_logic_1_1_i_helyszin_logic.html", "interface_craft_camp_1_1_logic_1_1_i_helyszin_logic" ],
    [ "ILogic", "interface_craft_camp_1_1_logic_1_1_i_logic.html", "interface_craft_camp_1_1_logic_1_1_i_logic" ],
    [ "InonCRUDLOGIC", "interface_craft_camp_1_1_logic_1_1_inon_c_r_u_d_l_o_g_i_c.html", "interface_craft_camp_1_1_logic_1_1_inon_c_r_u_d_l_o_g_i_c" ],
    [ "ISzuloLogic", "interface_craft_camp_1_1_logic_1_1_i_szulo_logic.html", "interface_craft_camp_1_1_logic_1_1_i_szulo_logic" ],
    [ "ITaborLogic", "interface_craft_camp_1_1_logic_1_1_i_tabor_logic.html", "interface_craft_camp_1_1_logic_1_1_i_tabor_logic" ],
    [ "NonCRUDLogic", "class_craft_camp_1_1_logic_1_1_non_c_r_u_d_logic.html", "class_craft_camp_1_1_logic_1_1_non_c_r_u_d_logic" ],
    [ "SzuloLogic", "class_craft_camp_1_1_logic_1_1_szulo_logic.html", "class_craft_camp_1_1_logic_1_1_szulo_logic" ],
    [ "TaborLogic", "class_craft_camp_1_1_logic_1_1_tabor_logic.html", "class_craft_camp_1_1_logic_1_1_tabor_logic" ]
];