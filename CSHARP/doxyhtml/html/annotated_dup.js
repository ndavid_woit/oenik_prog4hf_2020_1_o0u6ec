var annotated_dup =
[
    [ "CraftCamp", null, [
      [ "Data", null, [
        [ "CraftCampDBEntities", "class_craft_camp_1_1_data_1_1_craft_camp_d_b_entities.html", null ],
        [ "Gyerekek", "class_craft_camp_1_1_data_1_1_gyerekek.html", "class_craft_camp_1_1_data_1_1_gyerekek" ],
        [ "Helyszinek", "class_craft_camp_1_1_data_1_1_helyszinek.html", "class_craft_camp_1_1_data_1_1_helyszinek" ],
        [ "Jelentkezesek", "class_craft_camp_1_1_data_1_1_jelentkezesek.html", "class_craft_camp_1_1_data_1_1_jelentkezesek" ],
        [ "Szulok", "class_craft_camp_1_1_data_1_1_szulok.html", "class_craft_camp_1_1_data_1_1_szulok" ],
        [ "Taborok", "class_craft_camp_1_1_data_1_1_taborok.html", "class_craft_camp_1_1_data_1_1_taborok" ]
      ] ],
      [ "Logic", "namespace_craft_camp_1_1_logic.html", "namespace_craft_camp_1_1_logic" ],
      [ "Repository", "namespace_craft_camp_1_1_repository.html", "namespace_craft_camp_1_1_repository" ]
    ] ]
];