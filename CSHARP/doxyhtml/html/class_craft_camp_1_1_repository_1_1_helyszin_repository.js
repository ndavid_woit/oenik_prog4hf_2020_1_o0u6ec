var class_craft_camp_1_1_repository_1_1_helyszin_repository =
[
    [ "HelyszinRepository", "class_craft_camp_1_1_repository_1_1_helyszin_repository.html#a390852511733452b9e7fff6ef1483ab9", null ],
    [ "GetOne", "class_craft_camp_1_1_repository_1_1_helyszin_repository.html#ab7c1c0aaecdbc4e274401f91ac01661b", null ],
    [ "Hozzaadas", "class_craft_camp_1_1_repository_1_1_helyszin_repository.html#a152fe80d0b0ccab20810b13beeb167c2", null ],
    [ "ID_Modositas", "class_craft_camp_1_1_repository_1_1_helyszin_repository.html#aff416d8a067d377f3fcc2c7fe664fc72", null ],
    [ "Maxhely_Modositas", "class_craft_camp_1_1_repository_1_1_helyszin_repository.html#a44c107f8ebacdbc263e9898e88b06ef8", null ],
    [ "Nev_Modositas", "class_craft_camp_1_1_repository_1_1_helyszin_repository.html#a15e7bcaf8a7c78395c7887cf82bbdbda", null ],
    [ "Telefonszam_Modositas", "class_craft_camp_1_1_repository_1_1_helyszin_repository.html#a8aaf0d285aa9d758315a810f65322c97", null ],
    [ "Telepules_Modositas", "class_craft_camp_1_1_repository_1_1_helyszin_repository.html#aa636ad0c948be1d179773e94fda11f0a", null ],
    [ "Torles", "class_craft_camp_1_1_repository_1_1_helyszin_repository.html#a2dac4feed93cef24441bcd6de2fec3c5", null ],
    [ "Utca_Modositas", "class_craft_camp_1_1_repository_1_1_helyszin_repository.html#a5b6cb3da8b9be3bb273f3c6e38123a2f", null ]
];