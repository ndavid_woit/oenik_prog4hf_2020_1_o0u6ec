# OENIK_PROG3_2019_2_O0U6EC
## CSHARP Project

Projekt címe: CraftCamp

Funkciólista:

- Szülők listázása / hozzáadása / módosítása / törlése
- Gyerekek listázása / hozzáadása / módosítása / törlése
- Táborok listázása / hozzáadása / módosítása / törlése
- Helyszínek listázása / hozzáadása / módosítása / törlése
- Jelentkezések (összerendelések) listázása / hozzáadása / módosítása / törlése
- Legyen lehetőségünk kiírni azokat a szülőket, akik fel vannak iratkozva hírlevélre
- Legyen lehetőségünk kiírni azt, hogy az egyes táborokat átlagosan milyen maximális létszámmal indították
- Legyen lehetőségünk kiírni azt, hogy az egyes táborok mely helyszíneken lesznek megrendezve
