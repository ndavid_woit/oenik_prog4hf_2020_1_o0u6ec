/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dávid
 */
@WebServlet(name = "Gyerek", urlPatterns = {"/Gyerek"})
public class Gyerek extends HttpServlet {
    static Random R=new Random();
    static Random name=new Random();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/xml;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String nev1=request.getParameter("nev1");
            String nev2=request.getParameter("nev2");
            String nev3=request.getParameter("nev3");
            
            String [] nevek = {nev1, nev2, nev3};
            
            String szId=request.getParameter("szId");
            if (szId == null){
                szId = "111333AA";
            }
            
            String[] generatedNames = new String[3];
            generatedNames[0] = "GeneratedKiller";
            generatedNames[1] = "CuteCat";
            generatedNames[2] = "RandomPlayer";
            
            out.println("<gyerekek>");
            for (int i = 0; i < 3; i++) {
            out.println("<gyerek>");
            switch((name.nextInt(3)+1)){
                case 1: out.println("<mc>GeneratedKiller"+i+"</mc>");
                    break;
                case 2: out.println("<mc>CuteCat"+i+"</mc>");
                    break;
                case 3: out.println("<mc>RandomPlayer"+i+"</mc>");
                    break;
                default: out.println("<mc>NoOne</mc>");
                    
            }
            if (nevek[i]==null || nevek[i].length() <= 3){
                out.println("<nev>Teszt Elek</nev>");
            }
            else{
                out.println("<nev>"+nevek[i]+"</nev>");
            }
            out.println("<szuloID>"+szId+"</szuloID>");
            out.println("<kor>"+( 7 + new Random().nextInt(Math.abs(15 - 7)))+"</kor>");
            out.println("</gyerek>"); 
            }
            out.println("</gyerekek>");
 
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
